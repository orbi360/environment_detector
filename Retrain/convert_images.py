import os
import imghdr
from PIL import Image
for subdir, dirs, files in os.walk('./'):
  for file in files:
    filee = subdir+'/'+file
    typee = imghdr.what(filee)
    if typee != 'jpeg':
      print(typee, filee) 
      if typee == 'webp':
        im = Image.open(filee).convert("RGB")
        im.save(filee,"jpeg")
