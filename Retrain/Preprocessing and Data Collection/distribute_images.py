import os
import sys
from google_street_images import GoogleStreet
from shutil import copyfile


if os.sep == '\\':  # only Linux
    print(os.sep)
    sys.exit(0)

# For 4 classifier
path_from_4 = ["/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/ClosedHome",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/ClosedNature",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/OpenNature",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/OpenStreet"]

path_to_4 = ["/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_4/ClosedHome",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_4/ClosedNature",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_4/OpenNature",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_4/OpenStreet"]
           
# For inside/outside classifier
path_from_2 = ["/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/ClosedHome",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/ClosedNature",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/OpenNature",
             "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/OpenStreet"]

path_to_2 = ["/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_2/inside",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_2/inside",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_2/outside",
           "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_2/outside"]



class Distribute:

    def __init__(self, path_from, path_to, symlink=True):
        self.path_from = path_from
        self.path_to = path_to
        self.symlink = symlink

    def create_dir(self):
        for path in self.path_to:
            if not os.path.exists(path):
                os.mkdir(path)
    
    def validation_dir_all(self, path="/mnt/623E39233E38F221/Users/dima_/Google Диск (dmitriy.fedorov@nu.edu.kz)/share",
                       path_all="/mnt/623E39233E38F221/Users/dima_/Google Диск (dmitriy.fedorov@nu.edu.kz)/validation/all",
                       take_one_sample=True, verbose=False):
        filelist = []
        for root, dirs, files in os.walk(path):
            filelist = filelist + [os.path.join(root,x) for x in files if x.endswith(('.png'))] 
        
        if take_one_sample:
            # filelist = filelist[0:4] # debuging
            sett = set()
            new_filelist = []
            for image in filelist:  # takes only one image from folder
                pathh = f"{'/'.join(image.split('/')[:-1])}"
                if pathh not in sett:
                    new_filelist.append(image)
                sett.add(pathh)
            filelist = new_filelist

        for i, frm in enumerate(filelist):
            dst = f"{path_all}{os.sep}{i}.png"
            if verbose:
                print(frm)
                print(dst)
                print()
            if not os.path.exists(dst):
                if self.symlink:
                    os.link(frm, dst)
                else:
                    copyfile(frm, dst)

    def main(self):
        self.create_dir()
        for from_, to_ in zip(self.path_from, self.path_to):
            print(from_, to_)
            print()
            for subdir, dirs, files in os.walk(from_):
                for file in files:
                    frm = subdir + '/' + file
                    dst = to_ + '/' + file.replace('%', '')[-40:]
                    # print(frm)
                    # print(dst)
                    if not os.path.exists(dst):
                        if self.symlink:
                            os.link(frm, dst)
                        else:
                            copyfile(frm, dst)
                    else:
                        # print(dst)
                        break


if __name__ == "__main__":
    # Distribute(path_from_2, path_to_2).main()
    # path = "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/part1"
    # train_folder_2 = "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_2/outside"
    # GoogleStreet(path, train_folder_2).sample(1000)
     Distribute(path_from_2, path_to_2).validation_dir_all(verbose=True)