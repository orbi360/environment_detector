import os
import sys
import imghdr
from PIL import Image

if os.sep == '\\':  # only Linux
    print(os.sep)
    sys.exit(0)

path = "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads"
if False:
  for subdir, dirs, files in os.walk(path):
    for file in files:
      filee = subdir+'/'+file
      typee = imghdr.what(filee)
      if typee != 'jpeg':
        print(typee, filee) 
        if typee == 'webp':
          im = Image.open(filee).convert("RGB")
          im.save(filee,"jpeg")


for subdir, dirs, files in os.walk(path):
  for file in files:
    filee = subdir+'/'+file
    try:
      im = Image.open(filee)
      typee = im.format.lower()
      
      if typee != 'jpeg':
        print(typee, filee) 
        if typee == 'webp':
          im = im.convert("RGB")
          im.save(filee,"jpeg")
    except OSError:
      print('Del', filee) 
      os.remove(filee)
      # sys.exit(0)