# Google street images dataset
# http://www.cs.ucf.edu/~aroshan/index_files/Dataset_PitOrlManh/
# remove unnessary images and shufle
import os
import sys
import cv2
import numpy as np
from shutil import copyfile


if os.sep == '\\':  # only Linux
    print(os.sep)
    sys.exit(0)


class GoogleStreet:

    def __init__(self, path, train_folder, symlink=True):
        self.path = path
        self.image_list = []
        self.train_folder = train_folder
        self.scan_and_remove()
        self.symlink = symlink

    def scan_and_remove(self):    
        image_list = self.image_list      
        for subdir, dirs, files in os.walk(self.path):
            for file in files:
                if "_5" in file or "_0" in file:
                    file_path = subdir+'/'+file
                    # print(file_path)
                    os.remove(file_path)
                    # break
                else:
                    file_path = subdir+'/'+file
                    image_list.append(file_path)

    def remove_icons(self): # remove GUI icons
        for image in self.image_list:
            print(image)
            img = cv2.imread(image)
            if img.shape[0] > 900:
                img = img[150:]
                print(img.shape)
                cv2.imwrite(image, img)
        # cv2.imshow('image', img)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        # break

    def sample(self, sample_size):
        subset = np.random.choice(self.image_list, sample_size, replace=False)
        # copy subset to training folder 
        for i, image in enumerate(subset):
            dst = f"{self.train_folder}/{image.split('/')[-1]}"
            # print(image)
            print(i, dst)
            if self.symlink:
                os.link(image, dst)
            else:
                copyfile(image, dst)


if __name__ == "__main__":
    path = "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/downloads/part1"
    train_folder = "/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/training_data_4/OpenStreet"
    GoogleStreet(path, train_folder).sample(1000)

