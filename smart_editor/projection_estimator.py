import numpy as np
import quaternion
import math
from canvas import *
from reflect_bounding_box import ReflectBoundingBox


def spherical_coords_to_3d(s: tuple):
    sin_theta = math.sin(s[1])
    cos_theta = math.cos(s[1])
    sin_phi = math.sin(s[0])
    cos_phi = math.cos(s[0])
    return sin_theta * sin_phi, cos_theta, sin_theta * cos_phi


def from_3d_to_spherical_coords(point: tuple):
    w = point[1] / math.sqrt(point[0] * point[0] + point[1] * point[1] + point[2] * point[2])
    result = (math.atan(point[0] / point[2]), math.acos(w))
    if result[0] < 0.0:
        return result[0] + math.pi * 2.0, result[1]
    return result


def smoothstep(edge0: float, edge1: float, x: float):
    t = min(max((x - edge0) / (edge1 - edge0), 0.0), 1.0)
    return t * t * (3.0 - 2.0 * t)


class CylinderProjection:
    def __init__(self, source_size: tuple, cylinder_top: float, cylinder_bottom: float, cylinder_area: float):
        self.source_size = source_size
        self.cylinder_top = cylinder_top
        self.cylinder_bottom = cylinder_bottom
        self.cylinder_area = cylinder_area

    def to_spherical_coords(self, image_point: tuple):
        angle_x = (image_point[0] / self.source_size[0]) * self.cylinder_area
        y = (image_point[1] / self.source_size[1]) * (self.cylinder_top - self.cylinder_bottom) + self.cylinder_bottom
        angle_y = math.atan2(1.0, y)
        return angle_x, angle_y

    def image_to_3d(self, image_point: tuple):
        return spherical_coords_to_3d(self.to_spherical_coords(image_point))

    def from_3d_to_image(self, point):
        l = math.sqrt(point[0] * point[0] + point[2] * point[2])
        p = (point[0] / l, point[1] / l, point[2] / l)

        y = (p[1] + self.cylinder_bottom) / (self.cylinder_bottom - self.cylinder_top)
        y = min(max(1.0 - y, 0.0), 1.0) * self.source_size[1]

        angle = 0.25 - math.atan2(p[2], p[0]) / (2.0 * math.pi)
        angle = math.fmod(angle, 1.0)
        k = (2.0 * math.pi) / self.cylinder_area
        angle *= k
        angle1 = angle + k
        if angle1 < 1.0:
            return [(angle * self.source_size[0], y), (angle1 * self.source_size[0], y)]
        return [(angle * self.source_size[0], y)]


class MixUVProjection:
    MIXED_START_ANGLE = math.pi / 2.0
    MIXED_END_ANGLE = math.pi

    def __init__(self, aspect: float, scale: float):
        self.aspect = aspect
        self.scale = scale

    @property
    def real_scale(self):
        return self.scale + 1.0 if self.scale > 0.0 else 1.0 / (1.0 - self.scale)

    @property
    def plain_view_angle(self):
        return 2.0 * math.atan(1.0 / self.real_scale)

    @staticmethod
    def plain_to_polar(plain: tuple):
        result = [math.atan2(plain[1], plain[0]), math.sqrt(plain[0] * plain[0] + plain[1] * plain[1])]
        if result[0] < 0.0:
            result[0] += math.pi * 2.0
        return result

    def uv_little_planet_to_3d(self, uv: tuple):
        plain_pos = (uv[0] * 2.0 - 1.0, (uv[1] * 2.0 - 1.0) * self.aspect)
        polar = self.plain_to_polar((plain_pos[1], plain_pos[0]))
        polar[1] /= self.real_scale * 2.0
        return spherical_coords_to_3d((polar[0], math.pi - 2.0 * math.atan(1.0 / polar[1])))

    def uv_plain_to_3d(self, uv: tuple):
        return uv[0] * 2.0 - 1.0, self.real_scale, (uv[1] * 2.0 - 1.0) * self.aspect

    def uv_to_3d(self, uv: tuple):
        view_angle = self.plain_view_angle
        coeff = smoothstep(self.MIXED_START_ANGLE, self.MIXED_END_ANGLE, view_angle)

        pos3d_little_planet = self.uv_little_planet_to_3d(uv)
        pos3d_plain = self.uv_plain_to_3d(uv)

        return np.add(np.multiply(pos3d_little_planet, 1.0 - coeff),
                      np.multiply(pos3d_plain, coeff))


MAX_ZOOM = 3.0
MIN_ZOOM = -5.0

class ProjectionEstimator:
    smooth = 0.1
    eps_zoom = 1e-1

    def __init__(self, source_projection: CylinderProjection):
        self.source_projection = source_projection
        self.canvas = Canvas(source_projection.source_size, (16 * 50, 9 * 50))
        self.reflect_h_line = ((math.pi * 2.0) / source_projection.cylinder_area) * \
                              source_projection.source_size[0]
        # self.canvas = Canvas((3840, 1920), (16 * 50, 9 * 50))

    def process(self, global_track: list):
        projections = []
        return projections

    @staticmethod
    def dir_to_rotation_matrix(dir: np.array):
        l = math.sqrt(np.dot(dir, dir))
        n_dir_y = np.multiply(dir, 1.0 / l)
        n_dir_x = np.cross(n_dir_y, (0.0, -1.0, 0.0))
        l = math.sqrt(np.dot(n_dir_x, n_dir_x))
        n_dir_x = np.multiply(n_dir_x, 1.0 / l)
        n_dir_z = np.cross(n_dir_x, n_dir_y)
        return np.array([n_dir_x, n_dir_y, n_dir_z])

    def get_image_points_from_projection(self, R, projection):
        points = [projection.uv_to_3d((0.0, 0.0)), projection.uv_to_3d((1.0, 0.0)),
                  projection.uv_to_3d((1.0, 1.0)), projection.uv_to_3d((0.0, 1.0))]

        points = [np.dot(R, p) for p in points]

        image_points = []
        for p in points:
            image_points += self.source_projection.from_3d_to_image(p)

        return image_points

    def _window_in_projection(self, R, projection, window):
        image_points = self.get_image_points_from_projection(R, projection)
        bb_projection = ReflectBoundingBox(self.reflect_h_line, image_points[0])
        for p in image_points[1:]:
            bb_projection.add_point(p)
        if not bb_projection.point_in((window[0], window[1])):
            return False
        if not bb_projection.point_in((window[2], window[1])):
            return False
        if not bb_projection.point_in((window[2], window[3])):
            return False
        if not bb_projection.point_in((window[0], window[3])):
            return False
        return True

    def binary_search_scale(self, R, projection, window):

        lower = MIN_ZOOM
        upper = MAX_ZOOM

        count_iters = 0

        while (upper - lower) > self.eps_zoom:
            mid = (lower + upper) * 0.5

            projection.scale = mid
            if self._window_in_projection(R, projection, window):
                lower = mid
            else:
                upper = mid
            count_iters += 1
        if lower == MIN_ZOOM:
            projection.scale = lower
        # print(f'binaru search number iterations: {count_iters}')

    def get_rotation_from_window(self, window):
        image_center = np.multiply(np.add(window[0:2], window[2:4]), 0.5)
        dir = spherical_coords_to_3d(self.source_projection.to_spherical_coords(image_center))
        return self.dir_to_rotation_matrix(dir)

    def get_scale_of_projection(self, R, window, delta_k: float = 0.5):
        projection = MixUVProjection(9 / 16, 5.0)

        if delta_k > 0.0:
            delta = min(window[2] - window[0], window[3] - window[1]) * delta_k

            d_window = window[0] - delta, window[1] - delta, window[2] + delta, window[3] + delta
        else:
            d_window = window

        self.binary_search_scale(R.transpose(), projection, d_window)

        return projection.scale

    def get_fit_projection(self, window, delta_k: float = 0.5):
        R = self.get_rotation_from_window(window)
        return R, self.get_scale_of_projection(R, window, delta_k)

    def get_projection_image(self, image, projection_params: tuple):
        self.canvas.set_image(image)
        R = quaternion.as_rotation_matrix(projection_params[0]).astype(np.float32)
        return self.canvas.get_projection(R, projection_params[1])

    def get_corner_dirs(self, projection_params: tuple):
        projection = MixUVProjection(9 / 16, projection_params[1])
        R = quaternion.as_rotation_matrix(projection_params[0])
        R = R.transpose()
        corner_dirs = [projection.uv_to_3d((0.0, 0.0)), projection.uv_to_3d((1.0, 0.0)),
                       projection.uv_to_3d((1.0, 1.0)), projection.uv_to_3d((0.0, 1.0))]
        corner_dirs = [np.dot(R, p) for p in corner_dirs]
        return corner_dirs

