from enum import Enum, unique
import numpy as np
import quaternion
import json


@unique
class PovType(int, Enum):
    PovFree = 0,
    RotationTrackObject = 1,
    RotationTrackCompas = 2,
    RotationLookHere = 4,
    RotationTrack = 1 + 2,
    Rotation = 4 + 1 + 2,
    PovProjection = 8,
    PovZoom = 16,
    PovStabilization = 32,
    PovAny = (4 + 1 + 2) + 8 + 16 + 32

class TrackInfo:
    def __init__(self, pov_type: PovType, start_frame_index: int, end_frame_index: int, values: list):
        assert (end_frame_index >= start_frame_index)
        assert ((end_frame_index - start_frame_index + 1) == len(values))
        assert (start_frame_index >= 0)

        self.type = pov_type
        self.startFrameIndex = start_frame_index
        self.endFrameIndex = end_frame_index
        self.values = values

        for q in values:
            assert(type(q) is np.quaternion)

    def toMap(self):
        return {
            'startFrame': str(self.startFrameIndex),
            'endFrame': str(self.endFrameIndex),
            'type': int(self.type),
            'values': [{ 'x': q.x, 'y': q.y, 'z': q.z, 'w': q.w } for q in self.values]
        }



class ProjectWriter:
    def __init__(self, video_path: str):
        self.zooms = {}
        self.rotations = []
        self.videoPath = video_path
        self.startFrameIndex = 0
        self.endFrameIndex = 0

    def _check_collisions(self, start_frame_index: int, end_frame_index: int) -> bool:
        for r in self.rotations:
            if r.startFrameIndex <= start_frame_index <= r.endFrameIndex:
                return True
            if r.startFrameIndex <= end_frame_index <= r.endFrameIndex:
                return True
        return False

    def add_track(self, track_info: TrackInfo):
        assert (not self._check_collisions(track_info.startFrameIndex, track_info.endFrameIndex))
        self.rotations.append(track_info)
        self.startFrameIndex = min(self.startFrameIndex, track_info.startFrameIndex)
        self.endFrameIndex = max(self.endFrameIndex, track_info.endFrameIndex)

    def look_at_here(self, index_frame: int, q: np.quaternion, zoom: float):
        self.add_track(TrackInfo(PovType.RotationLookHere, index_frame, index_frame, [q]))
        self.set_zoom(index_frame, zoom)

    def set_zoom(self, index_frame: int, zoom: float):
        self.zooms[index_frame] = zoom

    def export(self, out_path: str):
        project = {
            'sources': [{
                'guid': 0,
                'path': self.videoPath,
                'type': 2
            }],
            'samples': {
                'video': [
                    {
                        'sample': {
                            'startFrame': str(self.startFrameIndex),
                            'endFrame': str(self.endFrameIndex),
                            'rotation': {
                                'rotations': [r.toMap() for r in self.rotations],
                                'zooms': self.zooms
                            }
                        },
                        'sourceGuid': 0
                    }
                ]
            }
        }

        if out_path[-4:] == '.opf':
            path = out_path
        else:
            path = out_path + '.opf'

        with open(path, 'w') as file:
            json.dump(project, file, indent="  ")

if __name__ == "__main__":
    writer = ProjectWriter("D:/results/106ORBIV/30_0/106ORBIV.mp4")
    writer.set_zoom(0, -10.0)
    writer.add_track(TrackInfo(PovType.RotationTrack, 0, 10, [ np.quaternion(1, 0, 0, 0) for _ in range(11) ]))
    writer.set_zoom(11, -5.0)
    writer.look_at_here(15, np.quaternion(-1.0, 0.0, 0.0, 0.0), -3.0)
    writer.look_at_here(100, np.quaternion(1.0, 0.0, 0.0, 0.0), -10.0)
    writer.export("1")