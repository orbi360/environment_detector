#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function, absolute_import

import os
from timeit import time
import warnings
import sys
import cv2
import numpy as np
from PIL import Image
from ourYOLO import YOLO

from deep_sort import preprocessing
from deep_sort import nn_matching
from deep_sort.detection import Detection
from deep_sort.outtracker import Tracker
from tools import generate_detections as gdet
from deep_sort.detection import Detection as ddet
import colorsys

warnings.filterwarnings('ignore')

def sort_detections(detections):
    detections_dict = dict()
    for i in range(len(detections)):
        if(detections[i].yclass in detections_dict):
            detections_dict[detections[i].yclass].append(detections[i])
        else:
            detections_dict[detections[i].yclass] = []
            detections_dict[detections[i].yclass].append(detections[i])

    return detections_dict


class videotracker(object):
    def __init__(self):
        self.yolo = YOLO()
        # deep_sort
        self.model_filename = 'model_data/mars-small128.pb'
        self.encoder = gdet.create_box_encoder(self.model_filename, batch_size=1)
        self.max_cosine_distance = 0.3
        self.nn_budget = None
        self.nms_max_overlap = 1.0
        self.metric = nn_matching.NearestNeighborDistanceMetric("cosine", self.max_cosine_distance, self.nn_budget)
        self.tracker = Tracker(self.metric)


    def parseFrame(self, frame, frame_num):
        image = Image.fromarray(frame)
        boxs, yscores, yclasses = self.yolo.detect_image(image)
        # print("box_num",len(boxs))
        features = self.encoder(frame, boxs)

        # score to 1.0 here).
        detections = [Detection(bbox, 1.0, feature) for bbox, feature in zip(boxs, features)]
        for i, d in enumerate(detections):
            detections[i].yscore = yscores[i]
            detections[i].yclass = yclasses[i]
            detections[i].frame_num = frame_num

        # Run non-maxima suppression.
        boxes = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        indices = preprocessing.non_max_suppression(boxes, self.nms_max_overlap, scores)
        detections = [detections[i] for i in indices]

        # detections_dict = sort_detections(detections)
        out_array = []

        # for class_name in detections_dict:
        #     if(class_name in self.tracker_dict):
        #         tracker = self.tracker_dict[class_name]
        #     else:
        #         metric = nn_matching.NearestNeighborDistanceMetric("cosine", self.max_cosine_distance, self.nn_budget)
        #         tracker = Tracker(metric)
        #         self.tracker_dict[class_name] = tracker


            # detections = detections_dict[class_name]
        self.tracker.predict()
        self.tracker.update(detections)

        for track in self.tracker.tracks:
            if not track.is_confirmed() or track.time_since_update >= 1:
                continue
            bbox = track.to_tlbr()
            #cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 255, 255), 2)
            #print(frame_num)
            #print(track.track_id, track.yclass[frame_num], track.yscore[frame_num])
            #label = '{} {} {}'.format(track.track_id, track.yclass[frame_num], track.yscore[frame_num])
            #cv2.putText(frame, label, (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, (0, 255, 0), 2)

            out = dict()
            out["track_id"] = track.track_id
            out["yolo_id"] = str(track.yclass[frame_num])
            out["yolo_score"] = float(track.yscore[frame_num])
            rect = track.to_tlbr()
            rect = (float(rect[0]), float(rect[1]), float(rect[2]), float(rect[3]))
            out["rect"] = rect
            out_array.append(out)

        #for det in detections:
        #    bbox = det.to_tlbr()
        #    cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), (255, 0, 0), 2)

        #cv2.imshow('', frame)

        return out_array


    def drawOnFrame(self, frame, tracks):
        for track in tracks:
            #col = self.RGB_tuples[track["track_id"] % 10]
            col = (155, 255, 0)
            bbox = track["rect"]
            cv2.rectangle(frame, (int(bbox[0]), int(bbox[1])), (int(bbox[2]), int(bbox[3])), col, 2)
            label = f'{track["track_id"]} {track["yolo_id"]} {track["yolo_score"]:.2f}'
            cv2.putText(frame, label, (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200, col, 2)
