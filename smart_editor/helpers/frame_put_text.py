import cv2


def write_text(frame, text, offset=(20,30), color=(0, 0, 0), color_bg=(255,255,255), 
               font=cv2.FONT_HERSHEY_SIMPLEX, font_scale=1, thickness=2):
    text_offset_x, text_offset_y = offset
    text_width, text_height = cv2.getTextSize(text, font, fontScale=font_scale, thickness=thickness)[0]
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width - 2, text_offset_y - text_height - 2))
    cv2.rectangle(frame, box_coords[0], box_coords[1], color_bg, cv2.FILLED)
    cv2.putText(frame, text, (text_offset_x, text_offset_y), font, fontScale=font_scale, color=color, thickness=thickness)
    return frame


def write_text_multiline(frame, text, offset=(20,30), color=(0, 0, 0), color_bg=(255,255,255),
                         font=cv2.FONT_HERSHEY_SIMPLEX, font_scale=1, thickness=2, linesep_px=4):
    text_offset_x, text_offset_y = offset
    for line in text.split("\n"):
        frame = write_text(frame, line, (text_offset_x, text_offset_y), color, color_bg,
                           font=cv2.FONT_HERSHEY_SIMPLEX, font_scale=1, thickness=2)
        text_width, text_height = cv2.getTextSize(line, font, fontScale=font_scale, thickness=thickness)[0]
        text_offset_y += text_height + linesep_px 
    return frame