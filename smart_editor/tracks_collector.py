from collections import OrderedDict
import math
import numpy as np
import cv2
from project_writer import PovType
from projection_estimator import *
import quaternion
import json
import sys
from copy import copy, deepcopy
from reflect_bounding_box import ReflectBoundingBox
import scipy.signal
import scipy.ndimage.filters

class Track:
    def __init__(self, track_id: int, label: str):
        self.track_id = track_id
        self.label = label
        self.frames = {}

    def compute_priority(self, type_multipler: float, reflect_h_line: float):
        if len(self.frames) < 2:
            for frame_id in self.frames:
                self.frames[frame_id]['priority'] = 0.0
            return
        n_frames = len(self.frames)
        keys = sorted(list(self.frames.keys()))
        vels = []
        for index, current_key in enumerate(keys[:-1]):
            next_key = keys[index + 1]

            current_rect = self.frames[current_key]['rect']
            next_rect = self.frames[next_key]['rect']
            moved_next_rect = (next_rect[0] + reflect_h_line, next_rect[1], next_rect[2] + reflect_h_line, next_rect[3])

            vels1 = (np.subtract(current_rect[0:2], next_rect[0:2]),
                     np.subtract(current_rect[2:4], next_rect[2:4]))

            vels1 = [math.sqrt(v[0] * v[0] + v[1] * v[1]) for v in vels1]
            vel1 = (vels1[0] + vels1[1]) * 0.5

            vels2 = (np.subtract(current_rect[0:2], moved_next_rect[0:2]),
                     np.subtract(current_rect[2:4], moved_next_rect[2:4]))

            vels2 = [math.sqrt(v[0] * v[0] + v[1] * v[1]) for v in vels2]
            vel2 = (vels2[0] + vels2[1]) * 0.5

            size = (current_rect[2] - current_rect[0], current_rect[3] - current_rect[1])

            vel = min(vel1, vel2) * (2.0 / float(size[0] + size[1]))

            vels.append(vel)

        vels = scipy.ndimage.filters.gaussian_filter1d(vels, 20)

        for index, (current_key, vel) in enumerate(zip(keys[:-1], vels)):
            self.frames[current_key]['priority'] = vel * type_multipler * float(min(n_frames - index, 100))

        self.frames[keys[-1]]['priority'] = 0.0


class TracksCollector:
    numberWaitFrames = 90
    minTrackSize = int(30 * 1.5)

    def __init__(self, reflect_h_line: float):
        self.tracks = []
        self.frames = {}
        self.main_tracks = []
        self.main_frames = {}
        self.range = (0, 0)
        self.reflect_h_line = reflect_h_line
        with open("model_data/priorities.json", 'r') as file:
            self.priorities = json.loads(file.read())

    def process_data(self, data: list, preferred_size: float):

        self.range = [0, 0]

        for frameDict in data:
            frame_id = int(frameDict['frame_num'])
            if frame_id < self.range[0]:
                self.range[0] = frame_id
            if frame_id > self.range[1]:
                self.range[1] = frame_id
            self.frames[frame_id] = {}
            for f_track in frameDict['tracking']:
                track = self.find_track(f_track['track_id'],
                                        max(0, frame_id - self.numberWaitFrames),
                                        frame_id)
                if track is None:
                    track = Track(int(f_track['track_id']), str(f_track['yolo_id']))
                    self.tracks.append(track)
                track.frames[frame_id] = {
                    'score': f_track['yolo_score'],
                    'rect': f_track['rect']
                }
                self.frames[frame_id][track.track_id] = track
        self.frames = OrderedDict(sorted(self.frames.items()))
        for track in self.tracks:
            track.frames = OrderedDict(sorted(track.frames.items()))
        self.fill_loss()
        self.remove_repeated_tracks()
        self.remove_repeated_tracks()
        self.remove_repeated_tracks()
        self.compute_priority()
        self.find_main()
        self.find_nulls()

    def find_track(self, track_id: int, frame_id_begin: int, frame_id_end: int):
        for i in reversed(range(frame_id_begin, frame_id_end)):
            if i in self.frames:
                if track_id in self.frames[i]:
                    return self.frames[i][track_id]
        return None

    def remove_repeated_tracks(self):
        while True:
            removed_tracks = set()
            for track_1 in self.tracks:
                if track_1.track_id in removed_tracks:
                    continue
                merged_tracks = []
                for frame_id in self.frames:
                    if frame_id not in track_1.frames:
                        continue
                    track_1_in_frame = track_1.frames[frame_id]
                    rect_1 = track_1_in_frame['rect']
                    bb_1 = ReflectBoundingBox(self.reflect_h_line, (rect_1[0], rect_1[1]))
                    bb_1.add_point((rect_1[2], rect_1[3]))
                    frame_info = self.frames[frame_id]
                    track_ids = list(frame_info.keys())
                    for track_2_id in track_ids:
                        if track_2_id == track_1.track_id:
                            continue
                        if track_2_id in removed_tracks:
                            frame_info.pop(track_2_id)
                            continue
                        track_2 = frame_info[track_2_id]
                        if not track_1.label == track_2.label:
                            continue
                        track_2_in_frame = track_2.frames[frame_id]
                        rect_2 = track_2_in_frame['rect']
                        c_area = bb_1.get_collision_area(rect_2)
                        c = c_area / (bb_1.get_area() + ReflectBoundingBox.get_rect_area(rect_2))
                        if c > 0.3:
                            merged_tracks.append(track_2)
                            frame_info.pop(track_2_id)
                            break

                for track_2 in merged_tracks:
                    for frame_id in track_2.frames:
                        track_2_data = track_2.frames[frame_id]
                        rect_2 = track_2_data['rect']
                        score = track_2_data['score']
                        bb = ReflectBoundingBox(self.reflect_h_line, (rect_2[0], rect_2[1]))
                        bb.add_point((rect_2[2], rect_2[3]))
                        if frame_id in track_1.frames:
                            track_1_data = track_1.frames[frame_id]
                            bb.add_rect(track_1_data['rect'])
                            score = max(score, track_1_data['score'])
                        track_1.frames[frame_id] = {
                            'rect': bb.rect,
                            'score': score
                        }
                    removed_tracks.add(track_2.track_id)

            if len(removed_tracks) == 0:
                break
            tracks = []
            for track in self.tracks:
                if track.track_id in removed_tracks:
                    continue
                for frame_id in track.frames:
                    data = track.frames[frame_id]
                    rect = data['rect']
                    if rect[2] > self.reflect_h_line:
                        data['rect'] = rect[0] - self.reflect_h_line, rect[1], rect[2] - self.reflect_h_line, rect[3]
                tracks.append(track)
            self.tracks = tracks

    def _interpolate_rect(self, rect_a, rect_b, t: float):
        delta1 = math.fabs(rect_a[0] - rect_b[0]) + math.fabs(rect_a[2] - rect_b[2])
        delta2 = math.fabs(rect_a[0] - rect_b[0] - self.reflect_h_line) + \
                 math.fabs(rect_a[2] - rect_b[2] - self.reflect_h_line)
        if delta2 < delta1:
            rect_b = rect_b[0] + self.reflect_h_line, rect_b[1], rect_b[2] + self.reflect_h_line, rect_b[3]
        return [(a * (1.0 - t) + b * t) for a, b in zip(rect_a, rect_b)]

    def fill_loss(self):
        for track in self.tracks:
            interpolated_frames = {}
            keys = sorted(list(track.frames.keys()))
            for index, begin in enumerate(keys[:-1]):
                end = keys[index + 1]

                track_start = track.frames[begin]
                track_end = track.frames[end]

                for i in range(begin + 1, end):
                    k = (i - begin) / float(end - begin)
                    interpolated_frames[i] = {
                        'score': track_start['score'] * (1.0 - k) + track_end['score'] * k,
                        'rect': self._interpolate_rect(track_start['rect'], track_end['rect'], k)
                    }
                    self.frames[i][track.track_id] = track
            track.frames = OrderedDict(sorted(list(interpolated_frames.items()) + list(track.frames.items())))

    def compute_priority(self):
        for track in self.tracks:
            type_multipler = self.priorities[track.label] if track.label in self.priorities else 0.2
            track.compute_priority(type_multipler, self.reflect_h_line)

    def find_main(self):
        self.main_tracks = []
        self.main_frames = {}

        for track in self.tracks:
            if len(track.frames) > self.minTrackSize:
                self.main_tracks.append(track)
                for frame_id in track.frames:
                    if frame_id not in self.main_frames:
                        self.main_frames[frame_id] = {track.track_id: track}
                    else:
                        self.main_frames[frame_id][track.track_id] = track

    def find_nulls(self):
        self.nulls = []
        keys = sorted(list(self.main_frames.keys()))
        for index, begin in enumerate(keys[:-1]):
            end = keys[index + 1]
            if (end - begin) > 1:
                self.nulls.append((begin + 1, end))

    def draw_tracks(self, frame, frame_id: int):
        if not frame_id in self.frames:
            return
        frame_info = self.frames[frame_id]
        for track_id in frame_info:
            track = frame_info[track_id]
            track_info = track.frames[frame_id]
            rect = track_info['rect']
            tl = (int(rect[0]), int(rect[1]))
            br = (int(rect[2]), int(rect[3]))
            cv2.rectangle(frame, tl, br, (0, 255, 155))
            cv2.putText(frame, track.label, tl, 0, 5e-3 * 200, (0, 255, 0), 2)

    @staticmethod
    def get_center_of_rect(rect):
        return (rect[0] + rect[2]) * 0.5, (rect[1] + rect[3]) * 0.5

    def compute_global_track(self, projection_estimator: ProjectionEstimator,
                             default_projections,
                             #cap,
                             max_number_sector_frames: int = 90,
                             max_number_active_objects: int = 3,
                             smooth_q: float = 0.9,
                             smmoth_zoom: float = 0.85):
        global_track = []
        mix_projection = MixUVProjection(9 / 16, -5.0)
        actual_q, actual_zoom = None, None
        sector_frame_id = self.range[0]
        while sector_frame_id <= self.range[1]:
            sector_tracks = []  # track.track_id : (track, start_track_rect)
            sector_tracks_range = {}
            for frame_id in range(sector_frame_id, min(sector_frame_id + max_number_sector_frames, self.range[1] + 1)):
                if frame_id not in self.main_frames:
                    continue
                frame_info = self.main_frames[frame_id]
                for track_id in frame_info:
                    if track_id in sector_tracks_range:
                        sector_tracks_range[track_id] = sector_tracks_range[track_id][0], frame_id
                    else:
                        track = frame_info[track_id]
                        track_data = track.frames[frame_id]
                        sector_tracks.append((track, track_data['rect']))
                        sector_tracks_range[track_id] = (frame_id, frame_id)

            sector_global_track = []

            current_tracks = []  # track, start_track_rect, track_range

            max_priority = 0.0

            while len(sector_tracks) > 0 and len(current_tracks) < max_number_active_objects:
                best_index = -1
                for index, (track, start_track_rect) in enumerate(sector_tracks):
                    current_tracks_1 = copy(current_tracks)
                    current_tracks_1.append((track, start_track_rect, sector_tracks_range[track.track_id]))

                    start_bb = None
                    current_range = (sector_frame_id + max_number_sector_frames, sector_frame_id)
                    for track, start_track_rect, (track_start_frame_id, track_end_frame_id) in current_tracks_1:
                        if track_start_frame_id < current_range[0]:
                            current_range = track_start_frame_id, max(current_range[1], track_end_frame_id)
                            start_bb = ReflectBoundingBox(self.reflect_h_line,
                                                          (start_track_rect[0], start_track_rect[1]))
                            start_bb.add_point((start_track_rect[2], start_track_rect[3]))
                        elif track_start_frame_id == current_range[0]:
                            start_bb.add_rect(start_track_rect)
                            if track_end_frame_id > current_range[0]:
                                current_range = track_start_frame_id, track_end_frame_id
                        elif track_end_frame_id > current_range[1]:
                            current_range = current_range[0], track_end_frame_id

                    R, target_zoom = projection_estimator.get_fit_projection(start_bb.rect)
                    target_q = quaternion.from_rotation_matrix(R)

                    cur_q, cur_zoom = actual_q, actual_zoom
                    if cur_q is None:
                        cur_q, cur_zoom = target_q, target_zoom

                    cur_global_track = []
                    cur_priority = 0.0

                    for frame_id in range(sector_frame_id, current_range[0]):
                        cur_q = quaternion.slerp(cur_q, target_q, 1.0, 0.0, smooth_q)
                        #target_zoom = projection_estimator.get_scale_of_projection(quaternion.as_rotation_matrix(cur_q),
                        #                                                           start_bb.rect)
                        cur_zoom = cur_zoom * smmoth_zoom + target_zoom * (1.0 - smmoth_zoom)
                        cur_global_track.append((cur_q, cur_zoom, []))

                    frame_id = current_range[0]
                    while frame_id <= current_range[1]:
                        start_frame_id = frame_id
                        bb = ReflectBoundingBox(self.reflect_h_line)
                        frame_tracks = []
                        frame_track_data = []  # rect, priority
                        while True:
                            for track, _, _ in current_tracks_1:
                                if frame_id in track.frames:
                                    track_data = track.frames[frame_id]
                                    rect = track_data['rect']
                                    bb.add_rect(rect)
                                    frame_tracks.append(track)
                                    frame_track_data.append((rect, track_data['priority']))
                            if len(frame_tracks) > 0:
                                break
                            frame_id += 1
                        R, target_zoom = projection_estimator.get_fit_projection(bb.rect)
                        #R = projection_estimator.get_rotation_from_window(bb.rect)
                        target_q = quaternion.from_rotation_matrix(R)
                        for c_frame_id in range(start_frame_id, frame_id):
                            cur_q = quaternion.slerp(cur_q, target_q, 1.0, 0.0, smooth_q)
                            #target_zoom = projection_estimator.get_scale_of_projection(
                            #    quaternion.as_rotation_matrix(cur_q), bb.rect)
                            cur_zoom = cur_zoom * smmoth_zoom + target_zoom * (1.0 - smmoth_zoom)
                            cur_global_track.append((cur_q, cur_zoom, []))

                        cur_q = quaternion.slerp(cur_q, target_q, 1.0, 0.0, smooth_q)
                        #target_zoom = projection_estimator.get_scale_of_projection(
                        #        quaternion.as_rotation_matrix(cur_q), bb.rect)
                        cur_zoom = cur_zoom * smmoth_zoom + target_zoom * (1.0 - smmoth_zoom)
                        cur_global_track.append((cur_q, cur_zoom, frame_tracks))

                        mix_projection.scale = cur_zoom
                        m_proj_points = projection_estimator.get_image_points_from_projection(R.transpose(),
                                                                                              mix_projection)
                        proj_bb = ReflectBoundingBox(self.reflect_h_line)
                        for p in m_proj_points:
                            proj_bb.add_point(p)

                        s_priority = 0.0
                        for rect, priority in frame_track_data:
                            s_priority += bb.get_collision_area(rect) * priority
                        s_priority *= len(frame_track_data) / proj_bb.get_area()

                        prev_track_params = cur_global_track[-2] if len(cur_global_track) > 1 \
                            else global_track[frame_id - 1]
                        prev_corner_dirs = projection_estimator.get_corner_dirs(prev_track_params)
                        cur_corner_dirs = projection_estimator.get_corner_dirs((cur_q, cur_zoom))

                        for prev_dir, cur_dir in zip(prev_corner_dirs, cur_corner_dirs):
                            s_priority *= (np.dot(prev_dir, cur_dir) + 1.0) * 0.5

                        cur_priority += s_priority

                        # cap.set(cv2.CAP_PROP_POS_FRAMES, frame_id)
                        # ret, frame = cap.read()
                        #
                        # cv2.rectangle(frame, (int(proj_bb.rect[0]), int(proj_bb.rect[1])),
                        #                      (int(proj_bb.rect[2]), int(proj_bb.rect[3])),
                        #                      (255, 0, 55), 3)
                        # for rect, _ in frame_track_data:
                        #     cv2.rectangle(frame, (int(rect[0]), int(rect[1])),
                        #                          (int(rect[2]), int(rect[3])),
                        #                          (0, 0, 255), 3)
                        #
                        # mix_projection.scale = cur_zoom
                        # R, target_zoom = projection_estimator.get_fit_projection(bb.rect, 0.0)
                        # target_q = quaternion.from_rotation_matrix(R)
                        # proj = projection_estimator.get_projection_image(frame, (target_q, target_zoom))
                        # cv2.imshow('frame', frame)
                        # cv2.imshow('proj', proj)
                        # cv2.waitKey(33)

                        frame_id += 1

                    if cur_priority > max_priority:
                        max_priority = cur_priority
                        sector_global_track = cur_global_track
                        best_index = index
                if best_index < 0:
                    break
                track, start_track_rect = sector_tracks[best_index]
                current_tracks.append((track, start_track_rect, sector_tracks_range[track.track_id]))
                del sector_tracks_range[track.track_id]
                del sector_tracks[best_index]

            if len(sector_global_track) == 0:
                cur_q, cur_zoom = actual_q, actual_zoom
                if cur_q is None:
                    cur_q, cur_zoom = default_projections[sector_frame_id]

                for q, zoom in default_projections[sector_frame_id:
                                                   min(sector_frame_id + max_number_sector_frames, self.range[1] + 1)]:
                    cur_q = quaternion.slerp(cur_q, q, 1.0, 0.0, smooth_q)
                    cur_zoom = cur_zoom * smmoth_zoom + zoom * (1.0 - smmoth_zoom)
                    sector_global_track.append((cur_q, cur_zoom, []))

            actual_q, actual_zoom, _ = sector_global_track[-1]

            global_track += sector_global_track
            sector_frame_id += len(sector_global_track)

        return global_track
