import matplotlib.pyplot as plt
from matplotlib.path import Path
import pathlib as pl
import matplotlib.patches as patches
import numpy as np
import json
import os


class VideoProcessor:
    def __init__(self):
        self.rotations = []
        self.velocities = []
        self.distances = []

        self.filename_360_video = None
        self.filename_cylinder_video = None
        self.filename_dynamic_info = None

    def process(self, video_filename: str, output_path: str):
        if video_filename[-4:] == ".mp4":
            self.filename_360_video = video_filename
        else:
            self.filename_360_video = output_path + ".mp4"

        self.filename_cylinder_video = output_path + "_CILINDRIC.mp4"
        self.filename_dynamic_info = output_path + "_dynamicInfo.json"

        if pl.Path(self.filename_360_video).exists() and \
            pl.Path(self.filename_cylinder_video).exists() and \
            pl.Path(self.filename_dynamic_info).exists():
            print("Use previous result")
        else:
            os.environ["LD_LIBRARY_PATH"] = (os.getcwd() + "/libs")
            cmd = 'cd ' + os.getcwd() + '/bin/' + ' && ./qmlgui -i extensive -t neuro ' \
                  '-a "inputFile={0};output={1}"'.format(video_filename, output_path)
            print(cmd)
            output = os.popen(cmd).read()
            print(output)
        if not pl.Path(self.filename_360_video).exists() or \
                not pl.Path(self.filename_cylinder_video).exists() or \
                not pl.Path(self.filename_dynamic_info).exists():
            raise Exception("not found results")
        self.load_dynamic_data()

    def load_dynamic_data(self):
        with open(self.filename_dynamic_info, "r") as file:
            data = json.loads(file.read())
            data = data["data"]

            self.rotations = []
            self.velocities = []
            self.distances = []

            for e in data:
                r = np.reshape(e['rotation'], (3, 3))

                self.rotations.append(r)
                self.velocities.append(np.array(e['velocity']))
                self.distances.append(e['distance'])

    def get_frames_info(self):
        frames = []
        for r, v, d in zip(self.rotations, self.velocities, self.distances):
            frames.append((r, v, d))
        return frames

    def show_dynamic_data(self):
        points = [np.array([0, 0, 0])]
        for v, d in zip(self.velocities, self.distances):
            points.append(np.add(points[-1], np.array(v[0:3])))

        points2d = [[v[0], v[2]] for v in points]

        path = Path(points2d)
        apatch = patches.PathPatch(path,
                                   linewidth=1,
                                   facecolor='none',
                                   edgecolor='k')
        fig, ax = plt.subplots(1)

        ax.add_patch(apatch)

        (left, top), (right, bottom) = (np.min(points2d, 0), np.max(points2d, 0))

        delta = np.subtract((right, bottom), (left, top))

        (left, top) = np.add((left, top), np.multiply(delta, -0.1))
        (right, bottom) = np.add((right, bottom), np.multiply(delta, 0.1))

        ax.set_xlim((left, right))
        ax.set_ylim((top, bottom))

        plt.show()

