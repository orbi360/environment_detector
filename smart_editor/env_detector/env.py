import numpy as np
import cv2
import tensorflow as tf
import time
import os
import numpy as np

class env:

    def __init__(self, 
                 model_file=f'{os.getcwd()}/env_detector/bin/retrained_graph.pb',
                 label_file=f'{os.getcwd()}/env_detector/labels/retrained_labels.txt',
                 debug=False):
        self.graph = self.load_graph(model_file)
        self.labels = self.load_labels(label_file)
        self.labels_env = ['inside', 'outside']
        self.num_class = len(self.labels)

        self.input_layer = "input"
        self.output_layer = "final_result"
        self.input_name = "import/" + self.input_layer
        self.output_name = "import/" + self.output_layer
        self.input_operation = self.graph.get_operation_by_name(self.input_name);
        self.output_operation = self.graph.get_operation_by_name(self.output_name);
        self.sess = tf.Session(graph=self.graph)
        self.debug = debug
        if self.num_class == 2:
            assert 'inside' == self.labels[0] or 'close' == self.labels[0]
            assert 'outside' == self.labels[1] or 'open' == self.labels[1]
        if self.num_class == 4:
            assert 'closed' in self.labels[0] and 'closed' in self.labels[1]
            assert 'open' in self.labels[2] and 'open' in self.labels[3]

    @staticmethod
    def load_graph(model_file):
        graph = tf.Graph()
        graph_def = tf.GraphDef()
        with open(model_file, "rb") as f:
            graph_def.ParseFromString(f.read())
        with graph.as_default():
            tf.import_graph_def(graph_def)
        return graph

    # @staticmethod  # depresiated 
    # def read_tensor_from_cv2(image, input_height=224, input_width=224,
    #                                 input_mean=128, input_std=128):
    #     float_caster = tf.cast(image, tf.float32)
    #     dims_expander = tf.expand_dims(float_caster, 0)
    #     resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    #     normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    #     with tf.Session() as sess:
    #         result = sess.run(normalized)
    #     return result

    @staticmethod
    def read_tensor_from_cv2_np(image, input_height=224, input_width=224,
                                       input_mean=128, input_std=128):
        float_caster = image.astype(np.float32)
        resized = cv2.resize(float_caster,(input_width, input_height), interpolation=cv2.INTER_LINEAR)
        normalized = np.divide(np.subtract(resized, [input_mean]), [input_std])
        result = np.expand_dims(normalized, 0)
        return result
    
    @staticmethod
    def load_labels(label_file):
        label = []
        proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
        for l in proto_as_ascii_lines:
            label.append(l.rstrip())
        return label

    def network_output(self, t):
        input_operation = self.input_operation
        output_operation = self.output_operation
        results = self.sess.run(output_operation.outputs[0], {input_operation.outputs[0]: t})
        return np.squeeze(results)

    def make_prediction(self, t): 
        results = self.network_output(t)

        if self.num_class == 4:
            method = 1
            labels_envplus = self.labels
            labels_env = self.labels_env
            top_k = results.argsort()[-len(results):][::-1]
            ENV_plus = (results[top_k[0]], labels_envplus[top_k[0]])
            if method == 0:  # sum probablities 
                results_env = np.array([np.sum(results[0:2]), np.sum(results[2:4])]) 
                top_k = results_env.argsort()[-len(results_env):][::-1]
                ENV = (results_env[top_k[0]], labels_env[top_k[0]])
            elif method == 1: # takes max probability
                if top_k[0] <= 1:
                    ENV = (results[top_k[0]], labels_env[0])
                else:
                    ENV = (results[top_k[0]], labels_env[1])
            return ENV, ENV_plus, results

        elif self.num_class == 2:
            labels_env = self.labels
            top_k = results.argsort()[-len(results):][::-1]
            ENV = (results[top_k[0]], labels_env[top_k[0]])
            return ENV, None, results

    def describe_frame(self, ENV, ENV_plus):
        to_return = {"ENV": ENV[1],
                     "ENV_confidence": str(ENV[0])[:4]}
        if ENV_plus is not None:
            to_return["ENV+"] = ENV_plus[1] # "OpenNature/OpenStreet/ClosedNature/ClosedHome"
            to_return["ENV+_confidence"] = str(ENV_plus[0])[:4]
        else:
            to_return["ENV+"] = 'None' # "OpenNature/OpenStreet/ClosedNature/ClosedHome"
            to_return["ENV+_confidence"] = 'None'
        return to_return

    @staticmethod
    def cut_frame(frame, pi=3):
        return frame[:,:int(frame.shape[1]*2//pi),:]

    def frame2dict(self, frame, pi=3,
                     input_height=224, input_width=224,
                     input_mean=128, input_std=128):
        if pi > 2:
            frame = self.cut_frame(frame, pi)
        # print(frame.shape)
        # cv2.imshow("frame", cv2.resize(frame,(224,224), interpolation = cv2.INTER_LINEAR))
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()
        t = self.read_tensor_from_cv2_np(frame,
                                         input_height=input_height,
                                         input_width=input_width,
                                         input_mean=input_mean,
                                         input_std=input_std)
        ENV, ENV_plus, raw = self.make_prediction(t)

        to_return = self.describe_frame(ENV, ENV_plus)
        if self.debug:
            return to_return, raw
        else:
            return to_return

