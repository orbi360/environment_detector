from env_detector.env import env
import os


class env_combo:

    def __init__(self):
        self.env_inside = env()  # class object that handles environment detection
        self.env_close = env(f'{os.getcwd()}/env_detector/bin/env_graph_2_mobilenet_0.50_224.pb',
                             f'{os.getcwd()}/env_detector/labels/output_2_labels.txt')
        self.env_plus = env(f'{os.getcwd()}/env_detector/bin/env_graph_4_mobilenet_0.50_224.pb',
                            f'{os.getcwd()}/env_detector/labels/output_4_labels.txt')

    def frame2dict(self, frame):
        out = self.env_inside.frame2dict(frame)
        out_close = self.env_close.frame2dict(frame)
        out_plus = self.env_plus.frame2dict(frame)
        out['ENV2'] = out_close['ENV']
        out['ENV2_confidence'] = out_close['ENV_confidence']
        out['ENV+'] = out_plus['ENV+']
        out['ENV+_confidence'] = out_plus['ENV+_confidence']
        return out
    
    def close(self):
        self.env_inside.sess.close()
        self.env_close.sess.close()
        self.env_plus.sess.close()