import os
import cv2
import sys
import json
import time
from helpers.defaults import argHandler
from helpers.frame_put_text import write_text_multiline
from videoTracker import videotracker
from env_detector.env_combo import env_combo
from tracks_collector import TracksCollector

def analyzeVideo(inputVideoFilename: str, FLAGS):
    scaleVideo = 0.6

    cap = cv2.VideoCapture(inputVideoFilename)
    cap.set(cv2.CAP_PROP_CONVERT_RGB, 1)
    startFrame = FLAGS.start_frame
    if startFrame == -1:
        startFrame = 0
    cap.set(cv2.CAP_PROP_POS_FRAMES, startFrame)
    currFrame = startFrame
    endFrame = FLAGS.end_frame
    if endFrame == -1:
        endFrame = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))

    if FLAGS.debug_video:
        outputVideoPath = FLAGS.debug_video_path
        if len(outputVideoPath) == 0:
            outputVideoPath = FLAGS.input + 'TRACKS.mp4'
        inputFrameSize = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        outCap = cv2.VideoWriter(outputVideoPath, fourcc, 20.0, inputFrameSize)
        print(outCap.isOpened())

    outDictionary = []
    lastEnvResult = ()

    tracker = videotracker()
    env_cmb = env_combo()  # class object that handles environment detection

    gstart = time.time()  # to measure Total runtime

    while cap.isOpened():
        start = time.time()  # for performance measurement
        currFrame = int(cap.get(1))
        frame_dict = {'frame_num': currFrame}
        ret, frame = cap.read()
        if not ret:
            break
        #skip_frame(cap)

        frame_dict['tracking'] = tracker.parseFrame(frame, currFrame)

        if (currFrame - startFrame) % FLAGS.env_period == 0:
            lastEnvResult = env_cmb.frame2dict(frame)
        frame_dict['ENV_detector'] = lastEnvResult

        if FLAGS.verbose:
            print(frame_dict)
        outDictionary.append(frame_dict)

        end = time.time()
        dt = end - start
        print(f'Frame {int(currFrame)}/{endFrame - startFrame} Time: {(end - gstart) / 60:.1f}m {dt:.3f}s {1 / dt:.1f} fps')

        if currFrame == endFrame:  # for debugging
            break

        if FLAGS.debug_video or FLAGS.debug_show:
            tracker.drawOnFrame(frame, frame_dict['tracking']) #(int(bbox[0]), int(bbox[1])),0, 5e-3 * 200, (0,255,0),2
            text = f"{lastEnvResult['ENV']}: {lastEnvResult['ENV_confidence']}\n" + \
                   f"{lastEnvResult['ENV2']}: {lastEnvResult['ENV2_confidence']}\n" + \
                   f"{lastEnvResult['ENV+']}: {lastEnvResult['ENV+_confidence']}"
            frame = write_text_multiline(frame, text, offset=(30, 30), color_bg=(0, 255, 0), font_scale=5e-3 * 200, thickness=2, linesep_px=8)
            # cv2.putText(frame, f'{lastEnvResult["ENV"]}: {lastEnvResult["ENV_confidence"]:.4}', (30, 30), 0, 5e-3 * 200, (0, 255, 0), 2)
        if FLAGS.debug_video:
            outCap.write(frame)
        if FLAGS.debug_show:
            frame = cv2.resize(frame, None, None, scaleVideo, scaleVideo)
            cv2.imshow('', frame)
            cv2.waitKey(1)

    print(f"Total time: {(time.time() - gstart)}")
    env_cmb.close()
    cap.release()
    outCap.release()
    return outDictionary


if __name__ == "__main__":
    FLAGS = argHandler()
    FLAGS.setEnvDefaults()
    # FLAGS.input = "/home/oosavu/qwe.mp4"
    FLAGS.input = "/mnt/623E39233E38F221/Users/dima_/Google Диск (dmitriy.fedorov@nu.edu.kz)/share/21 04 18 SF Church/111ORBIV/111ORBIV.CFGCILINDRIC.mp4"
    FLAGS.output = FLAGS.input + ".json"
    FLAGS.end_frame = 10
    FLAGS.parseArgs(sys.argv)
    analyzeVideo(FLAGS.input, FLAGS)

