import sys

class ReflectBoundingBox:
    def __init__(self, reflect_h_line: float, point: tuple = None):
        self.reflect_h_line = reflect_h_line
        if point is None:
            self.reset()
        else:
            x = point[0] + self.reflect_h_line if point[0] < 0.0 else point[0] - self.reflect_h_line \
                if point[0] > self.reflect_h_line else point[0]
            self.rect = x, point[1], x, point[1]

    def reset(self):
        self.rect = sys.float_info.max, sys.float_info.max, -sys.float_info.max, -sys.float_info.max

    @staticmethod
    def get_rect_area(rect) -> float:
        return max(rect[2] - rect[0], 0.0) * max(rect[3] - rect[1], 0.0)

    def get_area(self) -> float:
        return self.get_rect_area(self.rect)

    def add_rect(self, rect):
        min1, max1 = min(self.rect[0], rect[0]), max(self.rect[2], rect[2])
        min2, max2 = min(self.rect[0], rect[0] - self.reflect_h_line), \
                     max(self.rect[2], rect[2] - self.reflect_h_line)
        if (max2 - min2) < (max1 - min1):
            min1, max1 = min2, max2
        min2, max2 = min(self.rect[0], rect[0] + self.reflect_h_line), \
                     max(self.rect[2], rect[2] + self.reflect_h_line)
        if (max2 - min2) < (max1 - min1):
            min1, max1 = min2, max2

        if max1 < 0.0:
            min1 += self.reflect_h_line
            max1 += self.reflect_h_line
        if max1 > self.reflect_h_line:
            min1 -= self.reflect_h_line
            max1 -= self.reflect_h_line
        self.rect = min1, rect[1] if rect[1] < self.rect[1] else self.rect[1], \
                    max1, rect[3] if rect[3] > self.rect[3] else self.rect[3]

    def add_point(self, point):
        min1, max1 = min(self.rect[0], point[0]), max(self.rect[2], point[0])
        min2, max2 = min(self.rect[0], point[0] - self.reflect_h_line), \
                     max(self.rect[2], point[0] - self.reflect_h_line)
        if (max2 - min2) < (max1 - min1):
            min1, max1 = min2, max2
        min2, max2 = min(self.rect[0], point[0] + self.reflect_h_line), \
                     max(self.rect[2], point[0] + self.reflect_h_line)
        if (max2 - min2) < (max1 - min1):
            min1, max1 = min2, max2

        if max1 < 0.0:
            min1 += self.reflect_h_line
            max1 += self.reflect_h_line
        if max1 > self.reflect_h_line:
            min1 -= self.reflect_h_line
            max1 -= self.reflect_h_line
        self.rect = min1, point[1] if point[1] < self.rect[1] else self.rect[1], \
                    max1, point[1] if point[1] > self.rect[3] else self.rect[3]

    def get_collision_rect(self, rect):
        if rect[2] > self.rect[0] and rect[0] < self.rect[2]:
            r_min = max(rect[0], self.rect[0])
            r_max = min(rect[2], self.rect[2])
        else:
            min1, max1 = rect[0] - self.reflect_h_line, rect[2] - self.reflect_h_line
            if max1 > self.rect[0] and min1 < self.rect[2]:
                r_min = max(min1, self.rect[0])
                r_max = min(max1, self.rect[2])
            else:
                min1, max1 = rect[0] + self.reflect_h_line, rect[2] + self.reflect_h_line
                if max1 > self.rect[0] and min1 < self.rect[2]:
                    r_min = max(min1, self.rect[0])
                    r_max = min(max1, self.rect[2])
                else:
                    return None
        return r_min, rect[1] if rect[1] > self.rect[1] else self.rect[1], \
               r_max, rect[3] if rect[3] < self.rect[3] else self.rect[3]

    def get_collision_area(self, rect) -> float:
        c_rect = self.get_collision_rect(rect)
        return self.get_rect_area(c_rect) if c_rect is not None else 0.0

    def point_in(self, point: tuple) -> bool:
        if point[1] < self.rect[1] or point[1] > self.rect[3]:
            return False
        if point[0] < self.rect[0]:
            x = point[0] + self.reflect_h_line
        elif point[0] > self.rect[2]:
            x = point[0] - self.reflect_h_line
        else:
            return True
        return self.rect[0] <= x <= self.rect[2]

    def is_valid(self) -> bool:
        return self.rect[0] < self.rect[2] and self.rect[1] < self.rect[3]