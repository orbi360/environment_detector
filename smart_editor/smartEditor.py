import os
import cv2
import sys
import json
import time
import numpy as np
from helpers.defaults import argHandler
from tracks_collector import TracksCollector, ReflectBoundingBox
import neuroAnalyzer
from projection_estimator import ProjectionEstimator, CylinderProjection
from video_processor import VideoProcessor
import math, sys
import scipy.signal
import scipy.ndimage.filters
import quaternion
import pathlib

import matplotlib.pyplot as plt


def smooth(velocities):
    vel_x = scipy.ndimage.filters.gaussian_filter1d([v[0] for v in velocities], 15)
    vel_y = scipy.ndimage.filters.gaussian_filter1d([v[1] for v in velocities], 15)
    vel_z = scipy.ndimage.filters.gaussian_filter1d([v[2] for v in velocities], 15)
    return [(x, y, z) for x, y, z in zip(vel_x, vel_y, vel_z)]


def length_3d(v):
    return math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])


def normalize_3d(v):
    l = length_3d(v)
    if l < sys.float_info.epsilon:
        return None
    return v[0] / l, v[1] / l, v[2] / l


def quaternion_from_2_vectors(v_from, v_to):
    n_v_from = normalize_3d(v_from)
    n_v_to = normalize_3d(v_to)

    d = np.dot(n_v_from, n_v_to) + 1.0

    if d <= sys.float_info.epsilon:
        axis = np.cross((1.0, 0.0, 0.0), n_v_from)
        if length_3d(axis) <= sys.float_info.epsilon:
            axis = np.cross((0.0, 1.0, 0.0), n_v_from)
        axis = normalize_3d(axis)
        if axis is None:
            return None
        return quaternion.quaternion(0.0, axis[0], axis[1], axis[2])

    d = math.sqrt(2.0 * d)
    axis = np.cross(n_v_from, n_v_to)
    axis = (axis[0] / d, axis[1] / d, axis[2] / d)

    return quaternion.quaternion(d * 0.5, axis[0], axis[1], axis[2]).normalized()


def get_default_global_global_track(velocities, net_out, projection_estimator: ProjectionEstimator,
               plain_zoom = -1.5, little_planet_zoom: float = -4.0):
    def get_q_from_velocity(vel):
        l_axis = (vel[0], 0.0, vel[2])
        l_axis = normalize_3d(l_axis)
        if l_axis is None:
            return None
        return quaternion.quaternion(0.0, 1.0, 0.0, 0.0) * quaternion_from_2_vectors(l_axis, (0.0, 0.0, 1.0))

    new_global_track = []

    for vel, net in zip(velocities, net_out):
        if net['ENV_detector']['ENV'] == "outside":
            q = get_q_from_velocity(vel)
            zoom = little_planet_zoom
        else:
            q = quaternion.from_rotation_matrix(projection_estimator.dir_to_rotation_matrix(vel))
            zoom = plain_zoom
        new_global_track.append((q, zoom))
    return new_global_track

def convert_to_json_data(global_track: list, start_frame_id: int):
    map_global_track = {
        'startFrame': str(start_frame_id),
        'endFrame': str(start_frame_id + len(global_track)),
        'rotation': {
            'rotations': [],
            'zooms': []
        }
    }
    actual_tracks_ids = set()
    actual_qs = None
    actual_zooms = None
    actual_range = None
    for index, (q, zoom, track_list) in enumerate(global_track):
        frame_id = start_frame_id + index
        next_actual_tracks_ids = set()
        for track in track_list:
            next_actual_tracks_ids.add(track.track_id)
        if actual_tracks_ids == next_actual_tracks_ids:
            if actual_qs is not None:
                actual_qs.append(q)
                actual_zooms.append(zoom)
                actual_range = actual_range[0], frame_id
            else:
                actual_qs = [q]
                actual_zooms = [zoom]
                actual_range = frame_id, frame_id
        else:
            actual_tracks_ids = next_actual_tracks_ids
            if actual_qs is not None:
                map_global_track['rotation']['rotations'].append({
                    'startFrame': str(actual_range[0]),
                    'endFrame': str(actual_range[1]),
                    'type': 64,
                    'values': [{'x': -q.x, 'y': -q.y, 'z': -q.z, 'w': q.w} for q in actual_qs]
                })
                map_global_track['rotation']['zooms'].append({
                    'startFrame': str(actual_range[0]),
                    'endFrame': str(actual_range[1]),
                    'type': 16,
                    'values': actual_zooms
                })
            actual_qs = [q]
            actual_zooms = [zoom]
            actual_range = (frame_id, frame_id)
    if actual_qs is not None:
        map_global_track['rotation']['rotations'].append({
            'startFrame': str(actual_range[0]),
            'endFrame': str(actual_range[1]),
            'type': 64,
            'values': [{'x': -q.x, 'y': -q.y, 'z': -q.z, 'w': q.w} for q in actual_qs]
        })
        map_global_track['rotation']['zooms'].append({
            'startFrame': str(actual_range[0]),
            'endFrame': str(actual_range[1]),
            'type': 16,
            'values': actual_zooms
        })

    return map_global_track



def main(FLAGS):
    video_processor = VideoProcessor()
    video_processor.process(FLAGS.input, FLAGS.output)

    filename_net_out = FLAGS.output + "_net_out.json"
    if pathlib.Path(filename_net_out).exists():
        file = open(filename_net_out, "r")
        net_out = json.loads(file.read())
        file.close()
    else:
        net_out = neuroAnalyzer.analyzeVideo(video_processor.filename_cylinder_video, FLAGS)
        with open(filename_net_out, 'w') as file:
            json.dump(net_out, file, indent="  ")

    cap = cv2.VideoCapture(video_processor.filename_cylinder_video)
    #cap = cv2.VideoCapture("/home/alex/video_stab.mp4")
    width = cap.get(3)
    cap.set(cv2.CAP_PROP_CONVERT_RGB, 1)
    cap.set(cv2.CAP_PROP_POS_FRAMES, FLAGS.start_frame)

    frame_counter = 1
    gstart = time.time()  # to measure Total runtime

    ret, frame = cap.read()
    projection_estimator = ProjectionEstimator(CylinderProjection((frame.shape[1], frame.shape[0]), -1.0, 1.0, 7.85))

    dynamic_info = video_processor.get_frames_info()

    linear_velocities = [np.multiply(v[0:3], (1.0 / d if d > 0.0 else 1.0)) for r, v, d in dynamic_info]
    linear_velocities = smooth(linear_velocities)

    default_global_track = get_default_global_global_track(linear_velocities, net_out, projection_estimator)

    tracks_collector = TracksCollector(frame.shape[1] * (1.0 - 0.2))
    tracks_collector.process_data(net_out, 400)
    global_track = tracks_collector.compute_global_track(projection_estimator, default_global_track)

    scale_video = 0.6

    if FLAGS.show_output_video:
        while cap.isOpened():
            start = time.time()  # for performance measurement
            frame_num = int(cap.get(1))
            frame_dict = {'frame_num': frame_num}
            ret, frame = cap.read()
            if not ret:
                break

            q, zoom, current_tracks = global_track[frame_num]

            linear_velocity = linear_velocities[frame_num]
            proj_image = projection_estimator.get_projection_image(frame, (q, zoom))

            bb = ReflectBoundingBox(frame.shape[1] * (1.0 - 0.2))
            for track in current_tracks:
                window = track.frames[frame_num]['rect']
                bb.add_rect(window)
                window = (int(window[0]), int(window[1]), int(window[2]), int(window[3]))
                cv2.rectangle(frame, (window[0], window[1]), (window[2], window[3]), (0, 255, 0), 5)
                cv2.putText(frame, track.label, (window[0], window[1]-10), 0, 5e-3 * 200, (0, 255, 125), 2)
            if bb.is_valid():
                window = (int(bb.rect[0]), int(bb.rect[1]), int(bb.rect[2]), int(bb.rect[3]))
                cv2.rectangle(frame, (window[0], window[1]), (window[2], window[3]), (255, 0, 55), 5)
                window = (int(bb.rect[0] + bb.reflect_h_line), int(bb.rect[1]),
                          int(bb.rect[2] + bb.reflect_h_line), int(bb.rect[3]))
                cv2.rectangle(frame, (window[0], window[1]), (window[2], window[3]), (255, 0, 55), 5)
            cv2.imshow('proj', proj_image)

            if length_3d(linear_velocity) < 0.002:
                cv2.putText(frame, "static", (0, 30), 0, 5e-3 * 200, (125, 255, 0), 2)
            else:
                cv2.putText(frame, "dynamic", (0, 30), 0, 5e-3 * 200, (255, 155, 0), 2)

            if frame_counter == FLAGS.end_frame:  # for debugging
                break
            frame_counter += 1

            frame = cv2.resize(frame, None, None, scale_video, scale_video)
            cv2.imshow('', frame)
            cv2.imshow('proj', proj_image)
            cv2.waitKey(33)
            #if len(current_tracks) > 0:
            #    cv2.waitKey(-1)

    project = convert_to_json_data(global_track, tracks_collector.range[0])
    if FLAGS.output != "":
        with open(FLAGS.output + "_project.json", 'w') as file:
            json.dump(project, file, indent="  ")


if __name__ == "__main__":
    FLAGS = argHandler()
    FLAGS.setEnvDefaults()
    FLAGS.setYoloDefaults()
    FLAGS.setOtherDefaults()
    FLAGS.parseArgs(sys.argv)
    main(FLAGS)
