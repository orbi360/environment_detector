import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches
import numpy as np
import json
import os

class DynamicProcessor:
    def __init__(self):
        self.rotations = []
        self.velocities = []
        self.distances = []

    def run(self, video_filename: str, data_filepath: str = "dynamicData.json"):
        os.environ["LD_LIBRARY_PATH"] = (os.getcwd() + "/libs")
        print(os.environ.get("LD_LIBRARY_PATH"))
        cmd = './bin/qmlgui -i extensive -t calculateDynamicInfo ' \
              '-a "inputFile={0};outputFile={1}"'.format(video_filename, data_filepath)
        output = os.popen(cmd).read()
        print(output)
        self.load(data_filepath)

    def load(self, data_filepath: str = "dynamicData.json"):
        with open(data_filepath, "r") as file:
            data = json.loads(file.read())
            data = data["data"]

            self.rotations = []
            self.velocities = []
            self.distances = []

            for e in data:
                r = np.reshape(e['rotation'], (3, 3))

                self.rotations.append(r)
                self.velocities.append(np.array(e['velocity']))
                self.distances.append(e['distance'])

    def getFramesInfo(self):
        frames = []
        for r, v, d in zip(self.rotations, self.velocities, self.distances):
            frames.append({
                "rotation" : r,
                "velocity" : v,
                "distance" : d
            })
        return frames

    def show(self):
        points = [np.array([0, 0, 0])]
        for v, d in zip(self.velocities, self.distances):
            points.append(np.add(points[-1], np.array(v[0:3])))

        points2d = [[v[0], v[2]] for v in points]

        path = Path(points2d)
        apatch = patches.PathPatch(path,
                                   linewidth=1,
                                   facecolor='none',
                                   edgecolor='k')
        fig, ax = plt.subplots(1)

        ax.add_patch(apatch)

        (left, top), (right, bottom) = (np.min(points2d, 0), np.max(points2d, 0))

        delta = np.subtract((right, bottom), (left, top))

        (left, top) = np.add((left, top), np.multiply(delta, -0.1))
        (right, bottom) = np.add((right, bottom), np.multiply(delta, 0.1))

        ax.set_xlim((left, right))
        ax.set_ylim((top, bottom))

        plt.show()