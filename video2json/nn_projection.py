import tensorflow as tf

class NN_Projector:
    def __init__(self, session: tf.Session):
        self.buildModel(session)

    def buildModel(self, sesstion: tf.Session):
        self.session = sesstion
        self.


def doEpoch(nn_editor: NN_Editor, videoPath: str, data_filepath):

    with open(data_filepath, "r") as file:
        f_data = json.loads(file.read())
        totalFrameNumber = f_data["totalFrameNumber"]
        data = f_data["output"]

    cap = cv2.VideoCapture(videoPath)

    gstart = time.time()  # to measure Total runtime

    ret, frame = cap.read()
    frame_counter = 1

    while(cap.isOpened()):
        start = time.time()  # for performance measurement
        ret, frame = cap.read()

        if frame_counter >= totalFrameNumber: # for debugging
            break

        yolo_result = data[frame_counter]["YOLO"]

        canvas.reset()
        bg = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        bg = np.multiply(bg, 1.0 / 255.0)
        canvas.setImage(bg)

        state = []

        for r in yolo_result:
            if float(r["confidence"]) < 0.2:
                continue

            m_br = r["bottomright"]
            br = (m_br["x"], m_br["y"])
            m_tl =  r["topleft"]
            tl = (m_tl["x"], m_tl["y"])

            cv2.rectangle(frame, br, tl, (255, 0, 0), 3)
            canvas.drawRectangle(1.0, tl, br)
            state.append(tl[0])
            state.append(tl[1])
            state.append(br[0])
            state.append(br[1])

        if len(state) > 5 * 4:
            state = state[0:5*4]
        else:
            while len(state) < 5 * 4:
                state.append(0)
                state.append(0)
                state.append(0)
                state.append(0)

        #x = nn_editor.getProjection(state)

        x = (math.pi / 2.0, 0.0, 0.0, -5.0)

        r = (x[0], x[1], x[2])
        scale = max(min(x[3], -1.0), 1.0)

        proj = canvas.getProjection(exp_rotationMatrix(r), scale)
        cv2.imshow("proj", proj)
        score = np.sum(proj) / float(proj.size)
        error = nn_editor.train(x, state, score)
        print("score = {0}, error = {1}".format(score, error))

        frame_counter += 1
        # utility functions
        end = time.time()
        dt = end-start
        print("Frame {0}/{1} Time: {2}s, dt = {3}s, Fps = {4}".
              format(frame_counter, totalFrameNumber, (end-gstart)/60.0, dt, 1.0/dt))

        #cv2.imshow("frame", frame)
        cv2.waitKey(3)

    cap.release()


if __name__ == "__main__":
	# videoPath = input("Enter the path to video: ")
    videoPath = "/home/alex/191.mp4"  # fixed for debugging purposes
    data_filepath = videoPath + ".json"

    nn_editor = NN_Editor()

    for i in range(100):
        doEpoch(nn_editor, videoPath, data_filepath)