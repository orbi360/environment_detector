import os
import cv2
import json
import time
from pprint import pprint
from env_detector.env import env
from yolo.yolo import Yolo

if __name__ == "__main__":
    outDictionary = {}
	# videoPath = input("Enter the path to video: ")
    videoPath = "/home/alex/191.mp4"  # fixed for debugging purposes
    cap = cv2.VideoCapture(videoPath)
    totalFrameNumber = min(cap.get(cv2.CAP_PROP_FRAME_COUNT), 30 * 30)
    outDictionary['totalFrameNumber'] = int(totalFrameNumber)
    outDictionary['fps'] = int(cap.get(cv2.CAP_PROP_FPS))  # float objects are not json serializable
    outDictionary["output"] = []

    env = Env()  # class object that handles environment detection
    yolo = Yolo()  # class object that handles yolo
    frame_counter = 1
    gstart = time.time()  # to measure Total runtime

    while(cap.isOpened()):
        start = time.time()  # for performance measurement
        ret, frame = cap.read()
        frame_dict = {'frame_num': frame_counter}

        if ret == True:
            #frame = frame[...,::-1]  #convert BRG to RGB, due to inner working of OpenCV
            frame_dict['ENV_detector'] = env.frame2dict(frame)
            frame_dict["YOLO"] = yolo.frame2dict(frame)
        else:
            break

        if frame_counter > totalFrameNumber: # for debugging
            break
        
        outDictionary["output"].append(frame_dict)
        frame_counter += 1
        # utility functions
        end = time.time()
        dt = end-start
        print("Frame {0}/{1} Time: {2}s, dt = {3}s, Fps = {4}".
              format(frame_counter, totalFrameNumber, (end-gstart)/60.0, dt, 1.0/dt))

        cv2.imshow("frame", frame)
        cv2.waitKey(13)
    
    cap.release()
    # pprint(outDictionary)
    with open(videoPath + '.json', 'w') as fp:
        json.dump(outDictionary, fp, indent="  ")    

