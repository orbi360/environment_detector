import tensorflow as tf
from neurons_network import *

class NN_Editor:
    maxNumberObjects = 5

    def __init__(self):
        self._policy_model = NeuronsNetwork("Policy", self.maxNumberObjects * 4,
                                           [(30, TypeOutFuncs.Relu),
                                            (4, TypeOutFuncs.Null)])
        self._reward_model = NeuronsNetwork("Reward", 4 + self.maxNumberObjects * 4,
                                           [(30, TypeOutFuncs.Relu),
                                            (1, TypeOutFuncs.Null)])

        self._input_x = tf.placeholder(tf.float64, [None, 4])
        self._input_state = tf.placeholder(tf.float64, [None, self.maxNumberObjects * 4])

        self._input_reward = tf.concat([self._input_x, self._input_state], axis=1)

        self._reward = self._reward_model.getFinalVariable(self._input_reward)
        self._train_reward = tf.placeholder(tf.float64, [None, 1])
        self._reward_loss = tf.losses.mean_squared_error(self._reward, self._train_reward)
        reward_optimizer = tf.train.AdamOptimizer(0.1)
        self._reward_train_step = reward_optimizer.minimize(self._reward_loss, var_list=self._reward_model.var_list())

        self._policy_x = self._policy_model.getFinalVariable(self._input_state)
        self._policy_loss = tf.exp(- self._reward_model.getFinalVariable(tf.concat([self._policy_x, self._input_state], axis=1)))
        policy_optimizer = tf.train.AdamOptimizer(0.01)
        self._policy_train_step = policy_optimizer.minimize(self._policy_loss, var_list=self._policy_model.var_list())

        self._session = tf.InteractiveSession()
        self._session.run(tf.global_variables_initializer())

    def getProjection(self, input_state):
        x = self._session.run(self._policy_x,
                          { self._input_state: [input_state] })[0]
        return x

    def train(self, x, input_state, reward: float):
        self._session.run(self._reward_train_step,
                          { self._input_x: [x],
                            self._input_state: [input_state],
                            self._train_reward: [[reward]]})
        return self._session.run(self._reward_loss,
                          { self._input_x: [x],
                            self._input_state: [input_state],
                            self._train_reward: [[reward]]})

