import numpy as np
import cv2
import tensorflow as tf
import time
import json
import os
import numpy as np
# import pandas as pd
from pprint import pprint
from env import env
import glob


font = cv2.FONT_HERSHEY_SIMPLEX
font_scale = 1
thickness = 2


def write_text(frame, text, offset=(20,30)):
    global font_scale, thickness
    text_offset_x, text_offset_y = offset
    text_width, text_height = cv2.getTextSize(text, font, fontScale=font_scale, thickness=thickness)[0]
    box_coords = ((text_offset_x, text_offset_y), (text_offset_x + text_width - 2, text_offset_y - text_height - 2))
    cv2.rectangle(frame, box_coords[0], box_coords[1], (255,255,255), cv2.FILLED)
    cv2.putText(frame, text, (text_offset_x, text_offset_y), font, fontScale=font_scale, color=(0, 0, 0), thickness=thickness)
    return frame

def write_text_multiline(frame, text, offset=(20,30)):
    global font_scale, thickness
    text_offset_x, text_offset_y = offset
    for line in text.split("\n"):
        frame = write_text(frame, line, (text_offset_x, text_offset_y))
        text_width, text_height = cv2.getTextSize(line, font, fontScale=font_scale, thickness=thickness)[0]
        text_offset_y += text_height + 4
    return frame

class ValidateOnTestDataset:

    def __init__(self, path="/mnt/623E39233E38F221/Users/dima_/Google Диск (dmitriy.fedorov@nu.edu.kz)/share",
                 model_file="bin/env_graph_mobilenet_0.50_224.pb",
                 label_file="labels/output_labels.txt",
                 resize=True,
                 save_path='/mnt/B2D0C819D0C7E1A9/GDrive/Code Base/ImageNet_retrain/test_results_'):
        self.path = path
        self.env = env(model_file, label_file, debug=True)
        self.save_path = save_path + model_file.split(os.sep)[-1]
        self.resize = resize
        self.label_file_mask = "*.df" # "*.txt"
        if not os.path.exists(self.save_path):
            os.mkdir(self.save_path)

    def take_one_from_folder(self, filelist):
        sett = set()
        new_filelist = []
        for image in filelist:  # takes only one image from folder
            pathh = f"{'/'.join(image.split('/')[:-1])}"
            if pathh not in sett:
                new_filelist.append(image)
            sett.add(pathh)
        return new_filelist

    def scan_folders(self, score_all):
        filelist = []
        for root, dirs, files in os.walk(self.path):
            filelist = filelist + [os.path.join(root,x) for x in files if x.endswith(('.png'))] 
        if not score_all:
            filelist = self.take_one_from_folder(filelist)
        return filelist

    @staticmethod
    def get_true_labels(txt_label_file):
        with open(txt_label_file,'r') as fp: # read file that contains labels
            label = []
            vague = []
            for _ in range(3):
                line = fp.readline().strip()
                label.append(line.split(' ')[0])
                if 'vague' in line:
                    vague.append(True)
                else:
                    vague.append(False)
        return list(zip(label, vague))

    @staticmethod
    def check_update_error(out, true_labels, error_num, vague_error_num):
        error_bool = np.array([[False, False], [False, False], [False, False]])
        for i, info in enumerate(true_labels):
            label, vague = info
            if i == 0: # check inside
                error_bool[i] = [label != out['ENV'], vague]
            elif i == 1: # check closed
                # print(label)
                # label.replace('inside', 'open')
                # label.replace('outside', 'closed')
                label = label.replace('open', 'outside')
                label = label.replace('closed', 'inside')
                error_bool[i] = [label != out['ENV'], vague]
            elif i==2:
                error_bool[i] = [label.lower() != out['ENV+'], vague]
        
        for i, error in enumerate(error_bool):
            err, vag_err = error
            if err:
                error_num[i] += 1
            if vag_err:
                vague_error_num[i] += 1
        return error_num, vague_error_num, error_bool

    def score(self, score_all=False, interest=[0]):
        def print_error_bool(error_bool):
            def formatt(entry):
                a = 't' if not entry[0] else 'f'
                b = 'v' if entry[1] else ''
                return a+b
            return "_".join([formatt(entry) for entry in error_bool])
        def print_true_label(true_labels):
            def formatt(entry):
                b = '.v' if entry[1] else ''
                return f"{entry[0]}{b}"
            return ":".join([formatt(a) for a in true_labels])
        env = self.env
        filelist = self.scan_folders(score_all)
        # filelist = filelist[0:3]
        print(env.labels)
        total_num = len(filelist)
        error_num = np.zeros(3)
        vague_error_num = np.zeros(3)
        # check images
        for i, image in enumerate(filelist):
            img = cv2.imread(image)
            img = env.cut_frame(img, pi=3)
            out, raw = env.frame2PIdict(img, pi=2) # out-> JSON like, raw -> raw probabilities from network

            txt_label_file = glob.glob(f"{'/'.join(image.split('/')[:-1])}/{self.label_file_mask}")[0]
            true_labels = self.get_true_labels(txt_label_file)
            error_num, vague_error_num, error_bool = self.check_update_error(out, true_labels, error_num, vague_error_num)
            # print(error_num, vague_error_num, error_bool)
            # print(error_bool[:,0])
            if True in error_bool[:,0][interest]: # error_bool:
                print(f"{i}/{total_num} Error: {print_error_bool(error_bool)} {error_num} {vague_error_num}: {print_true_label(true_labels)}\n{out}\n")
                text = f"{out['ENV']}: {out['ENV_confidence']}\n{out['ENV+']}: {out['ENV+_confidence']}"
                
                if self.resize:
                    res = cv2.resize(img,(640,480), interpolation = cv2.INTER_LINEAR)
                res = write_text_multiline(res, text)
                res = write_text_multiline(res, print_true_label(true_labels), offset=(0, 470))

                cv2.imwrite(f'{self.save_path}/{i}.png', res)
                with open(f'{self.save_path}/{i}.txt','w') as fp:
                    fp.write(txt_label_file + '\n')
                    fp.write(image + '\n')
                    fp.write(f'True label: {true_labels}\n')
                    fp.write(str(env.labels) + '\n')
                    fp.write(str(raw) + '\n')

            # print(image)
            # print(txt_label_file)
            # print(true_label)
            # break
        print(f"Final score: {error_num/total_num}")
        print(f"Final score without vagur: {(error_num-vague_error_num)/total_num}")

    def score_old(self):
        env = self.env
        filelist = []
        for root, dirs, files in os.walk(self.path):
            filelist = filelist + [os.path.join(root,x) for x in files if x.endswith(('.png'))] 
        
        # filelist = filelist[0:4] # debuging
        filelist = self.take_one_from_folder(filelist)

        print(env.labels)
        total_num = len(filelist)
        error_num = 0
        # check images
        for i, image in enumerate(filelist):
            img = cv2.imread(image)
            out, raw = env.frame2dict(img) # out-> JSON like, raw -> raw probabilities from network
            txt_label_file = glob.glob(f"{'/'.join(image.split('/')[:-1])}/*.txt")[0]
            with open(txt_label_file,'r') as fp: # read file that contains labels
                true_label = fp.readline().strip()
            
            good_label = true_label == 'inside' or true_label == 'outside' # check for typos
            error_bool = out['ENV'] != true_label
            if error_bool and good_label:
                error_num += 1
            
            
            if error_bool: # error_bool:
                print(f"{i}/{total_num} Error: {error_num}: {true_label}, {out}")
                if not good_label:
                    print(f'bad {true_label}:, {txt_label_file}')
                text = f"{out['ENV']}: {out['ENV_confidence']}\n{out['ENV+']}: {out['ENV+_confidence']}"
                
                if self.resize:
                    res = cv2.resize(img,(640,480), interpolation = cv2.INTER_AREA)
                res = write_text_multiline(res, text)

                cv2.imwrite(f'{self.save_path}/{i}.png', res)
                with open(f'{self.save_path}/{i}.txt','w') as fp:
                    fp.write(txt_label_file + '\n')
                    fp.write(image + '\n')
                    if good_label:
                        fp.write(f'True label: {true_label}\n')
                    else: 
                        fp.write(f' Bad True label: {true_label}\n')
                    fp.write(str(env.labels) + '\n')
                    fp.write(str(raw) + '\n')

            # print(image)
            # print(txt_label_file)
            # print(true_label)
            # break
        print(f"{error_num/total_num:.3f}")


class ValidateOnVideo:

    def __init__(self, input_path, output_path,
                 start_frame=0, end_frame=-1,
                 resize=True,
                 model_file="bin/env_graph_mobilenet_0.50_224.pb",
                 label_file="labels/output_labels.txt",
                 resolution=(640,480)):
        
        self.cap = cv2.VideoCapture(input_path)
        self.cap.set(cv2.CAP_PROP_CONVERT_RGB, 1)
        self.cap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)
        self.resolution = resolution
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.out = cv2.VideoWriter(output_path, fourcc, 20.0, self.resolution)
        self.env = env(model_file, label_file, debug=True)
        # self.font = cv2.FONT_HERSHEY_SIMPLEX
        self.start_frame = start_frame
        self.end_frame = end_frame
        self.frame_scores = []
        self.output_path = output_path

    def score(self):
        cap = self.cap
        out = self.out
        frame_counter = self.start_frame
        totalFrameNumber = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        while cap.isOpened():
            
            ret, frame = cap.read()
            if not ret:
                break

            frame_dict, raw_scores = self.env.frame2dict(frame)
            self.frame_scores.append(raw_scores)
            text = f"{frame_dict['ENV']}: {frame_dict['ENV_confidence']}\n{frame_dict['ENV+']}: {frame_dict['ENV+_confidence']}"
            if self.resize: # optional resize
                frame = cv2.resize(frame, self.resolution, interpolation=cv2.INTER_AREA)
            frame = write_text_multiline(frame, text)

            print(f'Frame {int(frame_counter)}/{totalFrameNumber}')
            if frame_counter == self.end_frame:  # for debugging
                break
            
            out.write(frame)
            frame_counter += 1
        
        cap.release()
        out.release()

        summary_temp = np.array(self.frame_scores)
        mean = np.mean(self.frame_scores, axis=0)
        std = np.std(self.frame_scores, axis=0)
        print(mean)
        print(std)
        out_json = {}
        out_json['mean'] = [f"{element:.3f}" for element in mean]
        out_json['std'] = [f"{element:.3f}" for element in std]
        out_json["frames"] = [list(f"{element:.3f}" for element in row) for row in self.frame_scores]
        # pprint(out_json)
        # out_json = pd.DataFrame(out_json).to_json(orient='values')
        with open(self.output_path + '.json', 'w') as fp:
            json.dump(out_json, fp, indent="  ")

if __name__ == "__main__":
    test_num = 2
    if test_num == 0:
        ValidateOnTestDataset(model_file='bin/env_graph_2_mobilenet_0.50_224.pb',
                              label_file="labels/output_2_labels.txt").score(interest=[1])
    elif test_num == 1:
        ValidateOnTestDataset(model_file='bin/retrained_graph.pb',
                              label_file="labels/output_2_labels.txt").score(interest=[0])
    elif test_num == 2:
        ValidateOnTestDataset(model_file='bin/env_graph_4_mobilenet_0.50_224.pb',
                              label_file="labels/output_4_labels.txt").score(interest=[1, 2])                       
    # input_path = "/mnt/623E39233E38F221/Users/dima_/Google Диск (dmitriy.fedorov@nu.edu.kz)/share/23.02.18/104ORBIV/104ORBIV.CFGCILINDRIC.mp4"
    # output_path = "104ORBIV.CFGCILINDRIC.mp4"
    # ValidateOnVideo(input_path, output_path, end_frame=-1).score()
