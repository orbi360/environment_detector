import tensorflow as tf
import string
from copy import copy
from enum import IntEnum

class TypeOutFuncs(IntEnum):
    Null = 0,
    Relu = 1,
    Sigmoid = 2,
    Tanh = 3,
    ArgMax = 4,
    Logistic = 5

class NeuronsNetwork:

    def __init__(self, name: string, n_input : int, layers: list):
        assert(len(layers) > 0)

        self._name = copy(name)

        self._n_input = n_input
        self._layers = copy(layers)

        self._neurons = []
        self._var_list = []
        for i in range(len(self._layers)):
            layer = self._layers[i]
            assert(type(layer) is tuple and len(layer) == 2)
            assert(type(layer[0]) is int and layer[0] > 0)
            assert(type(layer[1]) is TypeOutFuncs)

            n_input = self._layers[i-1][0] if i > 0 else self._n_input
            n_output = layer[0]
            W = tf.Variable(tf.random_normal([ n_input, n_output ], 0.0, 1.0, dtype=tf.float64))
            b = tf.Variable(tf.random_normal([ n_output ], 0.0, 1.0, dtype=tf.float64))
            #W = tf.get_variable("{}_W{}".format(self._name, i), shape=[ n_input, n_output ], dtype=tf.float64)
            #b = tf.get_variable("{}_b{}".format(self._name, i), shape=[ n_output ], dtype=tf.float64)
            self._neurons.append((W, b))
            self._var_list.append(W)
            self._var_list.append(b)

    def name(self):
        return copy(self._name)

    def n_input(self):
        return self._n_input

    def n_output(self):
        return self._layers[-1][0]

    def layers(self):
        return copy(self._layers)

    def getActivationFunction(self, type_activation : TypeOutFuncs):
        if type_activation == TypeOutFuncs.Null:
            return lambda x: x
        elif type_activation == TypeOutFuncs.Relu:
            return tf.nn.relu
        elif type_activation == TypeOutFuncs.Sigmoid:
            return tf.nn.sigmoid
        elif type_activation == TypeOutFuncs.Tanh:
            return tf.nn.tanh
        elif type_activation == TypeOutFuncs.ArgMax:
            return tf.argmax
        elif type_activation == TypeOutFuncs.Logistic:
            return lambda x : 2.0 / (1.0 + tf.exp(-0.5 * x)) - 1.0
        else:
            raise TypeError("Invalid type of activation function")

    def var_list(self):
        return copy(self._var_list)

    def getFinalVariable(self, input : tf.Variable, name : string = None):
        if name is not None and self._name is not None:
            nameFunc = self._name + "_" + name
        else:
            nameFunc = None
        layers = []
        for i in range(len(self._layers)):
            nameLayer = name = nameFunc + "_layer{0}".format(i) if nameFunc is not None else None
            layer_input = layers[i-1] if i > 0 else input
            neuron = self._neurons[i]
            layer = tf.add(tf.matmul(layer_input, neuron[0]), neuron[1], nameLayer)
            layers.append(self.getActivationFunction(self._layers[i][1])(layer))
        return layers[-1]

