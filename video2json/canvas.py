import moderngl as gl
import numpy as np
import math
import cv2

# Eigen::Matrix<Type, 3, 1> ln_rotationMatrix(const Eigen::Matrix<Type, 3, 3> & rotationMatrix)
# {
#     static const Type m_sqrt1_2 = cast<Type>(0.707106781186547524401);
#
#     Eigen::Matrix<Type, 3, 1> result;
#
#     const Type cos_angle = (rotationMatrix(0, 0) + rotationMatrix(1, 1) + rotationMatrix(2, 2) - cast<Type>(1)) * cast<Type>(0.5);
#     result(0) = (rotationMatrix(2, 1) - rotationMatrix(1, 2)) * cast<Type>(0.5);
#     result(1) = (rotationMatrix(0, 2) - rotationMatrix(2, 0)) * cast<Type>(0.5);
#     result(2) = (rotationMatrix(1, 0) - rotationMatrix(0, 1)) * cast<Type>(0.5);
#
#     Type sin_angle_abs = std::sqrt(result(0) * result(0) + result(1) * result(1) + result(2) * result(2));
#     if (cos_angle > m_sqrt1_2) {            // [0 - Pi/4[ use asin
#         if (sin_angle_abs > cast<Type>(0)) {
#             result *= std::asin(sin_angle_abs) / sin_angle_abs;
#         }
#     } else if (cos_angle > - m_sqrt1_2) {    // [Pi/4 - 3Pi/4[ use acos, but antisymmetric part
#         const Type angle = std::acos(cos_angle);
#         result *= angle / sin_angle_abs;
#     } else {  // rest use symmetric part
#         // antisymmetric part vanishes, but still large rotation, need information from symmetric part
#         const Type angle = cast<Type>(M_PI) - std::asin(sin_angle_abs);
#         const Type d0 = rotationMatrix(0, 0) - cos_angle,
#             d1 = rotationMatrix(1, 1) - cos_angle,
#             d2 = rotationMatrix(2, 2) - cos_angle;
#         Eigen::Matrix<Type, 3, 1> r2;
#         if (((d0 * d0) > (d1 * d1)) && ((d0 * d0) > (d2 * d2))) { // first is largest, fill with first column
#             r2(0) = d0;
#             r2(1) = (rotationMatrix(1, 0) + rotationMatrix(0, 1)) * cast<Type>(0.5);
#             r2(2) = (rotationMatrix(0, 2) + rotationMatrix(2, 0)) * cast<Type>(0.5);
#         } else if ((d1 * d1) > (d2 * d2)) { 			    // second is largest, fill with second column
#             r2(0) = (rotationMatrix(1, 0) + rotationMatrix(0, 1)) * cast<Type>(0.5);
#             r2(1) = d1;
#             r2(2) = (rotationMatrix(2, 1) + rotationMatrix(1, 2)) * cast<Type>(0.5);
#         } else {							    // third is largest, fill with third column
#             r2(0) = (rotationMatrix(0, 2) + rotationMatrix(2, 0)) * cast<Type>(0.5);
#             r2(1) = (rotationMatrix(2, 1) + rotationMatrix(1, 2)) * cast<Type>(0.5);
#             r2(2) = d2;
#         }
#         // flip, if we point in the wrong direction!
#         if (r2.dot(result) < cast<Type>(0))
#             r2 *= cast<Type>(-1);
#         r2.normalize();
#         result = r2 * angle;
#     }
#     return result;
# }

def exp_rotationMatrix(w: tuple):
    def exp_rodrigues(w: tuple, A: float, B: float):
        rotation_matrix = np.zeros([3, 3])

        w_sq = [v * v for v in w[:3]]

        rotation_matrix[0][0] = 1.0 - B * (w_sq[1] + w_sq[2])
        rotation_matrix[1][1] = 1.0 - B * (w_sq[0] + w_sq[2])
        rotation_matrix[2][2] = 1.0 - B * (w_sq[0] + w_sq[1])

        a = A * w[2]
        b = B * (w[0] * w[1])
        rotation_matrix[0][1] = b - a
        rotation_matrix[1][0] = b + a

        a = A * w[1]
        b = B * (w[0] * w[2])
        rotation_matrix[0][2] = b + a
        rotation_matrix[2][0] = b - a

        a = A * w[0]
        b = B * (w[1] * w[2])
        rotation_matrix[1][2] = b - a
        rotation_matrix[2][1] = b + a

        return rotation_matrix

    one_6th = (1.0 / 6.0)
    one_20th = (1.0 / 20.0)

    theta_sq = np.dot(w[:3], w[:3])
    theta = math.sqrt(theta_sq)
    if theta_sq < 1e-6:
        A = 1.0 - one_6th * theta_sq
        B = 0.5
    else:
        if theta_sq < 1e-4:
            B = 0.5 - (0.25 * one_6th * theta_sq)
            A = 1.0 - (theta_sq * one_6th * (1.0 - one_20th * theta_sq))
        else:
            inv_theta = 1.0 / theta
            A = math.sin(theta) * inv_theta
            B = (1.0 - math.cos(theta)) * (inv_theta * inv_theta)
    return exp_rodrigues(w, A, B)

class Canvas:
    stablib = """
    #version 330
    
    float PI = 3.141592653589793238462643;
    float MIXED_START_ANGLE = PI/2.0;
    float MIXED_END_ANGLE = PI;

    float realZoomFromLinear(float linearZoom)
    {
        return (linearZoom > 0.0) ? (linearZoom + 1.0)  : (1.0 / (1.0-linearZoom));
    }
    
    vec2 plainToPolar(vec2 plain)
    {
        vec2 result =  vec2(atan(plain.y, plain.x), sqrt(plain.x * plain.x + plain.y * plain.y));
        if (result.x < 0.0)
            result.x += PI * 2.0;
    
        return result;
    }

    const float pixelOffset = 0.0;
    
    vec2 point3DtoSpherical(vec3 point)
    {
        float w = point.y / sqrt(point.x * point.x + point.y * point.y + point.z * point.z);
        vec2 result = vec2(atan(point.x, point.z), acos(w));
        if (result.x < 0.0)
             result.x += PI * 2.0;
    
        return result;
    }
    
    vec2 screenToSpherical(vec2 pixelPos, vec2 screenSize)
    {
        vec2 pixelCornerPos = pixelPos - vec2(pixelOffset);
        return pixelCornerPos * vec2(PI) / screenSize * vec2(2.0, 1.0);
    }
    
    vec2 sphericalToScreen(vec2 sphericalPos, vec2 screenSize)
    {
        return sphericalPos / vec2(2.0 * PI, PI) * screenSize + vec2(pixelOffset);
    }
    
    vec3 sphericalTo3D(vec2 spherical)
    {
        float phi = spherical.x;
        float theta = spherical.y;
        float sin_theta = sin(theta);
        float cos_theta = cos(theta);
        float sin_phi = sin(phi);
        float cos_phi = cos(phi);
        return vec3(sin_theta * sin_phi, cos_theta, sin_theta * cos_phi);
    }
    
    vec3 plain_unproject_to3d(vec2 normScreenPos, float aspect, float scale)
    {
        vec3 pos3d = vec3(0.0);
        pos3d.xz = normScreenPos * vec2(2.0) - vec2(1.0);
        pos3d.z *= aspect;
        pos3d.y = scale;
        return pos3d;
    }
    
    vec3 littlePlanet_unproject_to3d(vec2 normScreenPos, float aspect, float scale)
    {
        vec2 plainPos = normScreenPos * vec2(2.0) - vec2(1.0);
        plainPos.y *= aspect;
    
        vec2 polar = plainToPolar(plainPos.yx);
        polar.y /= (scale * 2.0);
        vec2 spherical = vec2(polar.x, PI - 2.0 * atan(1.0 / polar.y));
        return sphericalTo3D(spherical.xy);
    }
    
    float plainViewAngle(float scale)
    {
        return 2.0 * atan(1.0 / scale);
    }
    
    vec3 mixed_unproject_to3d(vec2 normScreenPos, float aspect, float scale)
    {
        float curAnlge = plainViewAngle(scale);
        float angleBegin = MIXED_START_ANGLE;
        float angleEnd =  MIXED_END_ANGLE;
        float coeff = smoothstep(angleBegin, angleEnd, curAnlge);
    
        vec3 pos3d_little_planet = littlePlanet_unproject_to3d(normScreenPos, aspect, scale);
        vec3 pos3d_plain = plain_unproject_to3d(normScreenPos, aspect, scale);
        return mix(pos3d_plain, pos3d_little_planet, vec3(coeff));
    }
    """

    def __init__(self, source_size: tuple, proj_size: tuple):

        self.ctx = gl.create_standalone_context()
        self.texture = self.ctx.texture(source_size, components=1, dtype='f4')
        self.fbo = self.ctx.framebuffer(self.texture)
        self.fbo_proj = self.ctx.simple_framebuffer(proj_size, components=1, dtype='f4')
        self.reset()
        self.programProj = self.ctx.program(
            vertex_shader='''
                #version 330
                in vec2 vert;
                void main() {
                    gl_Position = vec4(vert, 0.0, 1.0);
                }
            ''',
            fragment_shader = self.stablib + '''
                uniform int outWidth;
                uniform int outHeight;
                
                uniform float scale = -5.0;
                uniform mat3 rotation;
                
                uniform float phiArea = 7.85;
                uniform float top = 1.0;
                uniform float bottom = -1.0;
                
                uniform sampler2D Texture;
                
                out float result;
                
                float readFromCylinder(vec3 pos3d)
                {
                    vec3 p = pos3d / length(pos3d.xz);
                    float y = (p.y + bottom) / (bottom - top);
                    if ((y < 0.0) || (y > 1.0))
                        return 0.0;
                    
                    pos3d /= length(pos3d.xz);
                    float angle = 0.25 - atan(p.z, p.x) / (2.0 * PI);
                    angle = fract(angle);
                    float k = (2.0 * PI) / phiArea;
                    
                    angle *= k;
                    
                    float val = texture(Texture, vec2(angle, y)).r;
                    
                    float angle1 = angle + k;
                    if (angle1 < 1.0)
                        val = max(val, texture(Texture, vec2(angle1, y)).r);
                    
                    return val;
                }
                
                /*void main() {
                    vec2 s = screenToSpherical(gl_FragCoord.xy, vec2(outWidth, outHeight));
                    result = readFromCylinder(sphericalTo3D(s));  
                }*/
                
                void main() {
                    vec2 normScreenPos = gl_FragCoord.xy / vec2(outWidth, outHeight);
                    float aspect = float(outHeight) / float(outWidth);
                    float realScale = realZoomFromLinear(scale);
                    vec3 pos3d = plain_unproject_to3d(normScreenPos, aspect, realScale);
                    pos3d = rotation * pos3d;
                    result = readFromCylinder(pos3d);  
                }
                ''',)
        self.params = {'outWidth': self.programProj['outWidth'],
                       'outHeight': self.programProj['outHeight'],
                       'phiArea': self.programProj['phiArea'],
                       'top': self.programProj['top'],
                       'bottom': self.programProj['bottom'],
                       'scale': self.programProj['scale'],
                       'rotation': self.programProj['rotation']
                       }

        vertices = np.array([
            -1.0, -1.0,
             1.0, -1.0,
             1.0,  1.0,
            -1.0,  1.0
        ])
        self.vbo = self.ctx.buffer(vertices.astype('f4').tobytes())
        self.vao = self.ctx.simple_vertex_array(self.programProj, self.vbo, 'vert')


    def reset(self):
        self.fbo.clear()

    def drawRectangle(self, value: float, topleft: tuple, bottomright: tuple):
        self.fbo.clear(red=value, viewport=(topleft[0], topleft[1],
                                            bottomright[0] - topleft[0], bottomright[1] - topleft[1]))

    def getImage(self):
        self.fbo.use()
        bytes = self.fbo.read(components=1, dtype='f4')
        image = np.fromstring(bytes, dtype='f4').reshape([self.fbo.size[1], self.fbo.size[0], 1])
        return image

    def setImage(self, image):
        self.texture.write(image.astype('f4').tobytes())


    def getProjection(self, rotation, scale: float):
        self.fbo_proj.clear()
        self.fbo_proj.use()
        self.params['outWidth'].value = self.fbo_proj.size[0]
        self.params['outHeight'].value = self.fbo_proj.size[1]
        self.params['phiArea'].value = 7.85
        self.params['top'].value = 1.0
        self.params['bottom'].value = -1.0
        self.params['scale'].value = scale
        self.params['rotation'].write(rotation.astype('f4').tobytes())
        self.texture.use()

        self.vao.render(gl.TRIANGLE_FAN)
        bytes = self.fbo_proj.read(components=1, dtype='f4')
        image = np.fromstring(bytes, dtype='f4').reshape([self.fbo_proj.size[1], self.fbo_proj.size[0], 1])
        return image

if (__name__ == "__main__"):
    canvas = Canvas((300, 300), (200, 100))
    canvas.drawRectangle(1.0, (100, 100), (50, 50))
    canvas.drawRectangle(1.0, (0, 100), (50, 50))
    canvas.drawRectangle(1.0, (100, 0), (50, 50))
    canvas.drawRectangle(1.0, (200, 100), (50, 50))
    canvas.drawRectangle(1.0, (200, 200), (50, 50))
    canvas.drawRectangle(1.0, (100, 200), (50, 50))
    cv2.imshow("canvas", canvas.getImage())
    cv2.imshow("proj", canvas.getProjection(np.identity(3, dtype=np.float32), -5.0))
    cv2.waitKey()