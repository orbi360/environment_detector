from darkflow.net.build import TFNet
import os
from darkflow.defaults import argHandler

class Yolo:

    def __init__(self, 
                 model = "{0}/yolo/cfg/yolov2.cfg".format(os.getcwd()),
                 load = "{0}/yolo/bin/yolov2.weights".format(os.getcwd()),
                 threshold = 0.1):
        # print(os.getcwd(), model)
        FLAGS = argHandler()
        FLAGS.setDefaults()
        FLAGS["model"]  = model
        FLAGS["load"] = load
        FLAGS["threshold"] = threshold
        FLAGS["gpu"] = 0.7
        self.tfnet = TFNet(FLAGS)

    def frame2dict(self, frame):
        results_in_JSON = self.tfnet.return_predict(frame)
        for results in results_in_JSON:
            results['confidence'] = str(results['confidence'])
        return results_in_JSON
    
    